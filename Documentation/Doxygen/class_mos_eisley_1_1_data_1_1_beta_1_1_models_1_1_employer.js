var class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_employer =
[
    [ "Employer", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_employer.html#a1c6c4242df5ed075da47d8374d249fe6", null ],
    [ "MainData", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_employer.html#aa7924c61210c4ac2c59bec912a11a45f", null ],
    [ "Bounty", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_employer.html#a1c30a876b7d20914eadff5f36936d6fb", null ],
    [ "CreditsOwn", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_employer.html#a6d0598e612119890ec271af48f3b4b2a", null ],
    [ "Employees", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_employer.html#a8a82271433c0c6d940887e1ebfeb974b", null ],
    [ "HairColor", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_employer.html#af04df378a31ec1d61a88cb4976a86c27", null ],
    [ "Homeworld", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_employer.html#a8c12838e0853f6de82106e9311cbad56", null ],
    [ "Identichip", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_employer.html#af3db07ea052ca67b22d556d39d980f22", null ],
    [ "Job", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_employer.html#a5a8cd1caae3756d3d2fa068dad51f435", null ],
    [ "JobId", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_employer.html#a278cb3437674ebc701b83e635edeaa7b", null ],
    [ "Name", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_employer.html#a95226e93cf849ddefba82c0e7ec8a92e", null ],
    [ "Spaceship", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_employer.html#aa9fede8a5fc9c06ba7e9a625c8b657a6", null ],
    [ "SpaceshipId", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_employer.html#a9d3a188f3228a88338250308f8f696b2", null ]
];