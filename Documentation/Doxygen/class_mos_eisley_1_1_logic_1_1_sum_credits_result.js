var class_mos_eisley_1_1_logic_1_1_sum_credits_result =
[
    [ "Equals", "class_mos_eisley_1_1_logic_1_1_sum_credits_result.html#a9ced1adeeb8e1865f432a0a1cffa59fc", null ],
    [ "GetHashCode", "class_mos_eisley_1_1_logic_1_1_sum_credits_result.html#a3588351cdec05f6ab94b44f964ea034b", null ],
    [ "ToString", "class_mos_eisley_1_1_logic_1_1_sum_credits_result.html#ab2fc0228a04a9986afbfc99ab5c72d6a", null ],
    [ "JobName", "class_mos_eisley_1_1_logic_1_1_sum_credits_result.html#a53512da285bcc5d4073b5bbbca20a1c5", null ],
    [ "SumCredits", "class_mos_eisley_1_1_logic_1_1_sum_credits_result.html#a80a68cb8249ab7c256b6548060aafb7e", null ]
];