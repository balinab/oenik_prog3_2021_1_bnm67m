var class_mos_eisley_1_1_logic_1_1_avg_speed_result =
[
    [ "Equals", "class_mos_eisley_1_1_logic_1_1_avg_speed_result.html#a83925bb9d600a7a52740aeebfb195ebe", null ],
    [ "GetHashCode", "class_mos_eisley_1_1_logic_1_1_avg_speed_result.html#a38497ead7233897d50f576c30069dd02", null ],
    [ "ToString", "class_mos_eisley_1_1_logic_1_1_avg_speed_result.html#afcd849ccdfbc841d5042a3267a274178", null ],
    [ "AverageSpeed", "class_mos_eisley_1_1_logic_1_1_avg_speed_result.html#a21453d660b573cacb8a571cd4e738fdd", null ],
    [ "HomeworldName", "class_mos_eisley_1_1_logic_1_1_avg_speed_result.html#aed0f083bd2b2c4c73868de61c3d4ae6f", null ]
];