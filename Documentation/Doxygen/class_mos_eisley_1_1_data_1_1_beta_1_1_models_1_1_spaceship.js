var class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_spaceship =
[
    [ "Spaceship", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_spaceship.html#a31d864192e1c7f28da476f19b0296d86", null ],
    [ "MainData", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_spaceship.html#a18a716efdd76b673565bc6d9d4852c18", null ],
    [ "Armament", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_spaceship.html#aee699867530147e0301fda19d23b3817", null ],
    [ "Crew", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_spaceship.html#a3459f27db94eacf4e2858d080b7a6e77", null ],
    [ "Employees", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_spaceship.html#a9e163ea3e35abcfbfef4579b23de73b9", null ],
    [ "Employers", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_spaceship.html#ac5e1e6b2c30e6a8bffa82f4fd7031f26", null ],
    [ "MaxSpeed", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_spaceship.html#aac5328bcf967ee3e43632de2d26f7846", null ],
    [ "Size", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_spaceship.html#ac58967ae9a5bebed002b708ba8b38abb", null ],
    [ "SpaceshipId", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_spaceship.html#a8a61327a23e1bce822591b65a8196574", null ],
    [ "SpaceshipName", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_spaceship.html#a2fa0b309d6f19a7df5f1e7bece5fb7fa", null ]
];