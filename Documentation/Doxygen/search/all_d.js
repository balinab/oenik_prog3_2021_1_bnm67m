var searchData=
[
  ['remove_102',['Remove',['../interface_mos_eisley_1_1_logic_1_1_i_spaceship_logic.html#aaa24f2c3e4cdfcd77b432b2f37b07e99',1,'MosEisley.Logic.ISpaceshipLogic.Remove()'],['../class_mos_eisley_1_1_logic_1_1_spaceship_logic.html#a1cd5452e934e72cb790e9cff1fb1b3d3',1,'MosEisley.Logic.SpaceshipLogic.Remove()'],['../interface_mos_eisley_1_1_repository_1_1_i_repo.html#a9e31099ce2aa72aa75cd6d4578f2987d',1,'MosEisley.Repository.IRepo.Remove()'],['../class_mos_eisley_1_1_repository_1_1_repo.html#a0acb533f43a02838cec1b105b14b8979',1,'MosEisley.Repository.Repo.Remove()']]],
  ['removeemployee_103',['RemoveEmployee',['../interface_mos_eisley_1_1_logic_1_1_i_person_logic.html#a0e76a0c611391e2495c8c65ce4138538',1,'MosEisley.Logic.IPersonLogic.RemoveEmployee()'],['../class_mos_eisley_1_1_logic_1_1_person_logic.html#ac368a68788a8add7ed3f37eb18447fe1',1,'MosEisley.Logic.PersonLogic.RemoveEmployee()']]],
  ['removeemployer_104',['RemoveEmployer',['../interface_mos_eisley_1_1_logic_1_1_i_person_logic.html#a066426d0d795fcb3d846204d9491b349',1,'MosEisley.Logic.IPersonLogic.RemoveEmployer()'],['../class_mos_eisley_1_1_logic_1_1_person_logic.html#ac52a1abd6eb9875fe61780d75f04eddf',1,'MosEisley.Logic.PersonLogic.RemoveEmployer()']]],
  ['removejob_105',['RemoveJob',['../interface_mos_eisley_1_1_logic_1_1_i_person_logic.html#ad5e235f8b586eeb7d9a4b8eff66c3200',1,'MosEisley.Logic.IPersonLogic.RemoveJob()'],['../class_mos_eisley_1_1_logic_1_1_person_logic.html#ae438d08c0adf6bb92b241c86252825f2',1,'MosEisley.Logic.PersonLogic.RemoveJob()']]],
  ['repo_106',['Repo',['../class_mos_eisley_1_1_repository_1_1_repo.html',1,'MosEisley.Repository.Repo&lt; T &gt;'],['../class_mos_eisley_1_1_repository_1_1_repo.html#aef6e4adc01bf69ac1edbd536dc9f91ef',1,'MosEisley.Repository.Repo.Repo()']]],
  ['repo_3c_20employee_20_3e_107',['Repo&lt; Employee &gt;',['../class_mos_eisley_1_1_repository_1_1_repo.html',1,'MosEisley::Repository']]],
  ['repo_3c_20employer_20_3e_108',['Repo&lt; Employer &gt;',['../class_mos_eisley_1_1_repository_1_1_repo.html',1,'MosEisley::Repository']]],
  ['repo_3c_20job_20_3e_109',['Repo&lt; Job &gt;',['../class_mos_eisley_1_1_repository_1_1_repo.html',1,'MosEisley::Repository']]],
  ['repo_3c_20spaceship_20_3e_110',['Repo&lt; Spaceship &gt;',['../class_mos_eisley_1_1_repository_1_1_repo.html',1,'MosEisley::Repository']]]
];
