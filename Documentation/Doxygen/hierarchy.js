var hierarchy =
[
    [ "MosEisley.Logic.AvgSkillResult", "class_mos_eisley_1_1_logic_1_1_avg_skill_result.html", null ],
    [ "MosEisley.Logic.AvgSpeedResult", "class_mos_eisley_1_1_logic_1_1_avg_speed_result.html", null ],
    [ "DbContext", null, [
      [ "MosEisley.Data.Beta.Models.ModelContext", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_model_context.html", null ]
    ] ],
    [ "MosEisley.Data.Beta.Models.Employee", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_employee.html", null ],
    [ "MosEisley.Data.Beta.Models.Employer", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_employer.html", null ],
    [ "MosEisley.Program.Factory", "class_mos_eisley_1_1_program_1_1_factory.html", null ],
    [ "MosEisley.Logic.IPersonLogic", "interface_mos_eisley_1_1_logic_1_1_i_person_logic.html", [
      [ "MosEisley.Logic.PersonLogic", "class_mos_eisley_1_1_logic_1_1_person_logic.html", null ]
    ] ],
    [ "MosEisley.Repository.IRepo< T >", "interface_mos_eisley_1_1_repository_1_1_i_repo.html", [
      [ "MosEisley.Repository.Repo< T >", "class_mos_eisley_1_1_repository_1_1_repo.html", null ]
    ] ],
    [ "MosEisley.Repository.IRepo< Employee >", "interface_mos_eisley_1_1_repository_1_1_i_repo.html", [
      [ "MosEisley.Repository.IEmployeeRepository", "interface_mos_eisley_1_1_repository_1_1_i_employee_repository.html", [
        [ "MosEisley.Repository.EmployeeRepository", "class_mos_eisley_1_1_repository_1_1_employee_repository.html", null ]
      ] ]
    ] ],
    [ "MosEisley.Repository.IRepo< Employer >", "interface_mos_eisley_1_1_repository_1_1_i_repo.html", [
      [ "MosEisley.Repository.IEmployerRepository", "interface_mos_eisley_1_1_repository_1_1_i_employer_repository.html", [
        [ "MosEisley.Repository.EmployerRepository", "class_mos_eisley_1_1_repository_1_1_employer_repository.html", null ]
      ] ]
    ] ],
    [ "MosEisley.Repository.IRepo< Job >", "interface_mos_eisley_1_1_repository_1_1_i_repo.html", [
      [ "MosEisley.Repository.IJobRepository", "interface_mos_eisley_1_1_repository_1_1_i_job_repository.html", [
        [ "MosEisley.Repository.JobRepository", "class_mos_eisley_1_1_repository_1_1_job_repository.html", null ]
      ] ]
    ] ],
    [ "MosEisley.Repository.IRepo< Spaceship >", "interface_mos_eisley_1_1_repository_1_1_i_repo.html", [
      [ "MosEisley.Repository.ISpaceshipRepository", "interface_mos_eisley_1_1_repository_1_1_i_spaceship_repository.html", [
        [ "MosEisley.Repository.SpaceshipRepository", "class_mos_eisley_1_1_repository_1_1_spaceship_repository.html", null ]
      ] ]
    ] ],
    [ "MosEisley.Logic.ISpaceshipLogic", "interface_mos_eisley_1_1_logic_1_1_i_spaceship_logic.html", [
      [ "MosEisley.Logic.SpaceshipLogic", "class_mos_eisley_1_1_logic_1_1_spaceship_logic.html", null ]
    ] ],
    [ "MosEisley.Data.Beta.Models.Job", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_job.html", null ],
    [ "MosEisley.Logic.Tests.PersonLogicTests", "class_mos_eisley_1_1_logic_1_1_tests_1_1_person_logic_tests.html", null ],
    [ "MosEisley.Program.Program", "class_mos_eisley_1_1_program_1_1_program.html", null ],
    [ "MosEisley.Repository.Repo< Employee >", "class_mos_eisley_1_1_repository_1_1_repo.html", [
      [ "MosEisley.Repository.EmployeeRepository", "class_mos_eisley_1_1_repository_1_1_employee_repository.html", null ]
    ] ],
    [ "MosEisley.Repository.Repo< Employer >", "class_mos_eisley_1_1_repository_1_1_repo.html", [
      [ "MosEisley.Repository.EmployerRepository", "class_mos_eisley_1_1_repository_1_1_employer_repository.html", null ]
    ] ],
    [ "MosEisley.Repository.Repo< Job >", "class_mos_eisley_1_1_repository_1_1_repo.html", [
      [ "MosEisley.Repository.JobRepository", "class_mos_eisley_1_1_repository_1_1_job_repository.html", null ]
    ] ],
    [ "MosEisley.Repository.Repo< Spaceship >", "class_mos_eisley_1_1_repository_1_1_repo.html", [
      [ "MosEisley.Repository.SpaceshipRepository", "class_mos_eisley_1_1_repository_1_1_spaceship_repository.html", null ]
    ] ],
    [ "MosEisley.Logic.SameEmployeeAndEmployerHomeworldResult", "class_mos_eisley_1_1_logic_1_1_same_employee_and_employer_homeworld_result.html", null ],
    [ "MosEisley.Data.Beta.Models.Spaceship", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_spaceship.html", null ],
    [ "MosEisley.Logic.Tests.SpaceshipLogicTests", "class_mos_eisley_1_1_logic_1_1_tests_1_1_spaceship_logic_tests.html", null ],
    [ "MosEisley.Logic.SumCreditsResult", "class_mos_eisley_1_1_logic_1_1_sum_credits_result.html", null ],
    [ "MosEisley.Logic.Top3FastestShipOwnerResult", "class_mos_eisley_1_1_logic_1_1_top3_fastest_ship_owner_result.html", null ]
];