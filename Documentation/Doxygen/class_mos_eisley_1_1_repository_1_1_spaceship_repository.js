var class_mos_eisley_1_1_repository_1_1_spaceship_repository =
[
    [ "SpaceshipRepository", "class_mos_eisley_1_1_repository_1_1_spaceship_repository.html#a337ad0709253d35abcebb79c3dbb0aac", null ],
    [ "ChangeArmament", "class_mos_eisley_1_1_repository_1_1_spaceship_repository.html#a86c7f4ee77fa906f02062fc371d97dc5", null ],
    [ "ChangeCrew", "class_mos_eisley_1_1_repository_1_1_spaceship_repository.html#ab9f9652cbb239283ea3616dde1ddb9d4", null ],
    [ "ChangeMaximumSpeed", "class_mos_eisley_1_1_repository_1_1_spaceship_repository.html#acb90aab68639e71b20fc34f0c9e37cb6", null ],
    [ "ChangeSpaceshipName", "class_mos_eisley_1_1_repository_1_1_spaceship_repository.html#a3fac904e774c5a12a09103c76702ecef", null ],
    [ "GetOne", "class_mos_eisley_1_1_repository_1_1_spaceship_repository.html#a15122da57ed6ab3faa2869867194f0bb", null ]
];