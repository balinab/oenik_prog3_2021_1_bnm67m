var class_mos_eisley_1_1_logic_1_1_same_employee_and_employer_homeworld_result =
[
    [ "Equals", "class_mos_eisley_1_1_logic_1_1_same_employee_and_employer_homeworld_result.html#a53b60b92388bd4046746e29bdeed1dc1", null ],
    [ "GetHashCode", "class_mos_eisley_1_1_logic_1_1_same_employee_and_employer_homeworld_result.html#a492e794d36e112a9bf6192e8042d1230", null ],
    [ "ToString", "class_mos_eisley_1_1_logic_1_1_same_employee_and_employer_homeworld_result.html#af864c9fa8fb899f470774da8fd925fa8", null ],
    [ "EmployeeName", "class_mos_eisley_1_1_logic_1_1_same_employee_and_employer_homeworld_result.html#a0d106746e32b1cd5688fcb019614cd4d", null ],
    [ "EmployerName", "class_mos_eisley_1_1_logic_1_1_same_employee_and_employer_homeworld_result.html#a25e5d7a4e11c47f9beafd3ece799d5b7", null ],
    [ "SameHomeworld", "class_mos_eisley_1_1_logic_1_1_same_employee_and_employer_homeworld_result.html#a5d6c422c2221f50ae11d3f3224958177", null ]
];