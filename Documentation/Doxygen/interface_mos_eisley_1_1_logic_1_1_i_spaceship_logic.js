var interface_mos_eisley_1_1_logic_1_1_i_spaceship_logic =
[
    [ "ChangeArmament", "interface_mos_eisley_1_1_logic_1_1_i_spaceship_logic.html#a136f9ff51d21c0fa90684af0737e8f31", null ],
    [ "ChangeCrew", "interface_mos_eisley_1_1_logic_1_1_i_spaceship_logic.html#a25dc859b4b9216e00793aedc7f04812b", null ],
    [ "ChangeMaximumSpeed", "interface_mos_eisley_1_1_logic_1_1_i_spaceship_logic.html#a44590e7aea6f9900443b5e43713bfb25", null ],
    [ "ChangeSpaceshipName", "interface_mos_eisley_1_1_logic_1_1_i_spaceship_logic.html#a71227eeacbd11257a73231e6d0f60a78", null ],
    [ "GetAllSpaceships", "interface_mos_eisley_1_1_logic_1_1_i_spaceship_logic.html#a6ac98b2c72fbae42798a7ac96fee332e", null ],
    [ "GetSpaceshipById", "interface_mos_eisley_1_1_logic_1_1_i_spaceship_logic.html#ad192d7275042137f65b428edcf1cf69b", null ],
    [ "GetSpeedAvg", "interface_mos_eisley_1_1_logic_1_1_i_spaceship_logic.html#a4545adf4da5de8f63ff1bd1d2068291c", null ],
    [ "GetTop3FastestShipOwnerEmployees", "interface_mos_eisley_1_1_logic_1_1_i_spaceship_logic.html#ac42ba701cbc0405d479d199e5ff37b6e", null ],
    [ "Insert", "interface_mos_eisley_1_1_logic_1_1_i_spaceship_logic.html#a4707ff66e2131858871d7b86f365e7b6", null ],
    [ "Remove", "interface_mos_eisley_1_1_logic_1_1_i_spaceship_logic.html#aaa24f2c3e4cdfcd77b432b2f37b07e99", null ]
];