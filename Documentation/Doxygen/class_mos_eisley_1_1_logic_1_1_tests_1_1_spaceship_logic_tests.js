var class_mos_eisley_1_1_logic_1_1_tests_1_1_spaceship_logic_tests =
[
    [ "SetupTest", "class_mos_eisley_1_1_logic_1_1_tests_1_1_spaceship_logic_tests.html#a815bea060cedbf81fe7be0637a38bc25", null ],
    [ "TestChangeSpaceshipName", "class_mos_eisley_1_1_logic_1_1_tests_1_1_spaceship_logic_tests.html#a71c0e1f5d379fd8070dd3d0801526669", null ],
    [ "TestGetSpaceshipById", "class_mos_eisley_1_1_logic_1_1_tests_1_1_spaceship_logic_tests.html#a29b030a1a9b87e5b6fe5b9f9cba88742", null ],
    [ "TestGetSpeedAvg", "class_mos_eisley_1_1_logic_1_1_tests_1_1_spaceship_logic_tests.html#a609bf03243baeb99f7112b47cce5b0c5", null ],
    [ "TestGetTop3FastestShipOwnerEmployees", "class_mos_eisley_1_1_logic_1_1_tests_1_1_spaceship_logic_tests.html#aea74698e52233bfb2623d2ee0d468b3f", null ]
];