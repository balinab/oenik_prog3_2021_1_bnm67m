var class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_job =
[
    [ "Job", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_job.html#ad7423d69b0b9aa59a080f473da14bbd9", null ],
    [ "MainData", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_job.html#a2d8da54e84da5119f8cbc866c13936a4", null ],
    [ "Employees", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_job.html#a7bf569384a84b17b773c3e10ae57af9a", null ],
    [ "Employers", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_job.html#ab016820f5a61f1fe29bda681dcd9b4c5", null ],
    [ "JobDescription", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_job.html#aad6f1add031a78491fdf4fbe5b5e1d7b", null ],
    [ "JobId", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_job.html#a70e7cfd94423f58c16d2ed35b7f4791e", null ],
    [ "JobName", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_job.html#a6ce127af24dc0db211d72258a36c1716", null ],
    [ "JobRating", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_job.html#a8abee4fd2373af8f3422b925a49bbdaf", null ],
    [ "MaxWage", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_job.html#a3f3d313a358151a855984985a32d3008", null ],
    [ "MinWage", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_job.html#a5c429a70cba98b6458018c306bf6c95a", null ]
];