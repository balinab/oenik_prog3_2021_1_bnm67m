var class_mos_eisley_1_1_logic_1_1_spaceship_logic =
[
    [ "SpaceshipLogic", "class_mos_eisley_1_1_logic_1_1_spaceship_logic.html#a6c60562956644f801b15d3f5794c0c1d", null ],
    [ "ChangeArmament", "class_mos_eisley_1_1_logic_1_1_spaceship_logic.html#a0c4296c9a5e24e621d761b98c2779074", null ],
    [ "ChangeCrew", "class_mos_eisley_1_1_logic_1_1_spaceship_logic.html#ae0e4ed8ba07fa2ca7194842c89a3cd65", null ],
    [ "ChangeMaximumSpeed", "class_mos_eisley_1_1_logic_1_1_spaceship_logic.html#a24aa2e07cf85bc2d36f910f6a4bc11e3", null ],
    [ "ChangeSpaceshipName", "class_mos_eisley_1_1_logic_1_1_spaceship_logic.html#aadd40803e2acf17f1d12dbb9431076ca", null ],
    [ "GetAllSpaceships", "class_mos_eisley_1_1_logic_1_1_spaceship_logic.html#a558575f44ba476983ba2d4858dd87cd5", null ],
    [ "GetSpaceshipById", "class_mos_eisley_1_1_logic_1_1_spaceship_logic.html#acbf98116c3fa483c6a78c0fc3f349cbd", null ],
    [ "GetSpeedAvg", "class_mos_eisley_1_1_logic_1_1_spaceship_logic.html#a8cf1842101a9de3412f65b6cdd424402", null ],
    [ "GetSpeedAvgAsync", "class_mos_eisley_1_1_logic_1_1_spaceship_logic.html#a99133dac58e80f4896dd9a819c7d1a8d", null ],
    [ "GetTop3FastestShipOwnerEmployees", "class_mos_eisley_1_1_logic_1_1_spaceship_logic.html#a2e8a7d1ba921b28b19ecf748145da92a", null ],
    [ "GetTop3FastestShipOwnerEmployeesAsync", "class_mos_eisley_1_1_logic_1_1_spaceship_logic.html#a834529c3631fa1fa1db4d7748febbfd2", null ],
    [ "Insert", "class_mos_eisley_1_1_logic_1_1_spaceship_logic.html#a703b1d1180fd84627af1a5a82e1ee38f", null ],
    [ "Remove", "class_mos_eisley_1_1_logic_1_1_spaceship_logic.html#a1cd5452e934e72cb790e9cff1fb1b3d3", null ]
];