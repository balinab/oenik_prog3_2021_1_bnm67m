var interface_mos_eisley_1_1_repository_1_1_i_spaceship_repository =
[
    [ "ChangeArmament", "interface_mos_eisley_1_1_repository_1_1_i_spaceship_repository.html#abacb9b0dea18ab6a88a42cc0d00acbb4", null ],
    [ "ChangeCrew", "interface_mos_eisley_1_1_repository_1_1_i_spaceship_repository.html#af819d0dd80afa9c70c4f73e2118e970d", null ],
    [ "ChangeMaximumSpeed", "interface_mos_eisley_1_1_repository_1_1_i_spaceship_repository.html#a7b7f378a014a72d12b83b18f08044678", null ],
    [ "ChangeSpaceshipName", "interface_mos_eisley_1_1_repository_1_1_i_spaceship_repository.html#a4220e1e217fe25f1e44504ae3c5be4f4", null ]
];