var dir_f5c31fed606e40598cbedde69307fd73 =
[
    [ "obj", "dir_d6c5562a7ac2cae9e8796f8b7acbbfe4.html", "dir_d6c5562a7ac2cae9e8796f8b7acbbfe4" ],
    [ "AvgSkillResult.cs", "_avg_skill_result_8cs_source.html", null ],
    [ "AvgSpeedResult.cs", "_avg_speed_result_8cs_source.html", null ],
    [ "GlobalSuppressions.cs", "_mos___eisley_8_logic_2_global_suppressions_8cs_source.html", null ],
    [ "IPersonLogic.cs", "_i_person_logic_8cs_source.html", null ],
    [ "ISpaceshipLogic.cs", "_i_spaceship_logic_8cs_source.html", null ],
    [ "PersonLogic.cs", "_person_logic_8cs_source.html", null ],
    [ "SameEmployeeAndEmployerHomeworldResult.cs", "_same_employee_and_employer_homeworld_result_8cs_source.html", null ],
    [ "SpaceshipLogic.cs", "_spaceship_logic_8cs_source.html", null ],
    [ "SumCreditsResult.cs", "_sum_credits_result_8cs_source.html", null ],
    [ "Top3FastestShipOwnerResult.cs", "_top3_fastest_ship_owner_result_8cs_source.html", null ]
];