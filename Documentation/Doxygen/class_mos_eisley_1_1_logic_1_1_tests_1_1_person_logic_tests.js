var class_mos_eisley_1_1_logic_1_1_tests_1_1_person_logic_tests =
[
    [ "SetupTest", "class_mos_eisley_1_1_logic_1_1_tests_1_1_person_logic_tests.html#afedf0b8c69b11955999044388eb44564", null ],
    [ "TestChangeJobName", "class_mos_eisley_1_1_logic_1_1_tests_1_1_person_logic_tests.html#a96b99a69058ca0f79bc812fb30e4e960", null ],
    [ "TestChangeNameEmployee", "class_mos_eisley_1_1_logic_1_1_tests_1_1_person_logic_tests.html#abdb9cc4b25030571b94a08b152c20f47", null ],
    [ "TestChangeNameEmployer", "class_mos_eisley_1_1_logic_1_1_tests_1_1_person_logic_tests.html#a3a4ba240b9b17daebfd7d138aa3ac262", null ],
    [ "TestGetEmployeeById", "class_mos_eisley_1_1_logic_1_1_tests_1_1_person_logic_tests.html#ae9468040efed2939afc62e807eac1df0", null ],
    [ "TestGetEmployerById", "class_mos_eisley_1_1_logic_1_1_tests_1_1_person_logic_tests.html#a75fa447917400669c96b08d3844ab991", null ],
    [ "TestGetJobAvg", "class_mos_eisley_1_1_logic_1_1_tests_1_1_person_logic_tests.html#aa8a7d0d24f3886f8de4e2dcea737c8d8", null ],
    [ "TestGetJobById", "class_mos_eisley_1_1_logic_1_1_tests_1_1_person_logic_tests.html#a9ba658bea5345fa112749971c0e61fb1", null ],
    [ "TestGetJobSumCredits", "class_mos_eisley_1_1_logic_1_1_tests_1_1_person_logic_tests.html#ac55e7b5e297b087823944e2acf901594", null ],
    [ "TestSameEmployeeAndEmployerHomeworld", "class_mos_eisley_1_1_logic_1_1_tests_1_1_person_logic_tests.html#a76e74bf1872638255dff854a1cecca26", null ]
];