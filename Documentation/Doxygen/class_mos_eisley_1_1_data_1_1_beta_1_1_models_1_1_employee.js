var class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_employee =
[
    [ "MainData", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_employee.html#a3cc030774aabe9c8c250a74b4ebf1a10", null ],
    [ "Bounty", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_employee.html#a4bd081d283e9f774625b44d2c94706da", null ],
    [ "CreditsWanted", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_employee.html#ae25d03f23e4baa0cc85bb9b0e5b8f644", null ],
    [ "Employer", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_employee.html#af0ec05ff5c7fcb2692dff5f6b92bb328", null ],
    [ "EmployerId", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_employee.html#a5aef03f3920a720babf35067c239e27a", null ],
    [ "Homeworld", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_employee.html#a3481ed9a8d5d636f87c2370afdc1f988", null ],
    [ "Identichip", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_employee.html#a8d2537a66dfde559e7868d65a0415fbe", null ],
    [ "Job", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_employee.html#a2e8803c049a14661aa5111a427120c25", null ],
    [ "JobId", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_employee.html#afefc7220d2c1da017381d5de1c44616d", null ],
    [ "Name", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_employee.html#af76b7de3304f5923748732ca9e72bc00", null ],
    [ "Skills", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_employee.html#a76febfb9ded34307634c41e674bebe28", null ],
    [ "Spaceship", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_employee.html#a7ae1f94fe178026755c781f3ef384ebe", null ],
    [ "SpaceshipId", "class_mos_eisley_1_1_data_1_1_beta_1_1_models_1_1_employee.html#a4d2301f4d71163f6e66277813b21a5bb", null ]
];