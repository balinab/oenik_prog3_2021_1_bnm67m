var class_mos_eisley_1_1_logic_1_1_top3_fastest_ship_owner_result =
[
    [ "Equals", "class_mos_eisley_1_1_logic_1_1_top3_fastest_ship_owner_result.html#a80d86e71f1719d82a46a876c6227c644", null ],
    [ "GetHashCode", "class_mos_eisley_1_1_logic_1_1_top3_fastest_ship_owner_result.html#a2633c375501e838e3380727c2b623b66", null ],
    [ "ToString", "class_mos_eisley_1_1_logic_1_1_top3_fastest_ship_owner_result.html#a553766f70add467a49bb3cfc4117b936", null ],
    [ "MaximumSpeed", "class_mos_eisley_1_1_logic_1_1_top3_fastest_ship_owner_result.html#a7c52175055fe22b928c32b8e996d00c5", null ],
    [ "OwnerName", "class_mos_eisley_1_1_logic_1_1_top3_fastest_ship_owner_result.html#aa5467189d700caab9509f008814013c8", null ],
    [ "ShipName", "class_mos_eisley_1_1_logic_1_1_top3_fastest_ship_owner_result.html#a668aca30783a6f60c0178799fc81dccd", null ]
];