var namespace_mos_eisley_1_1_logic =
[
    [ "Tests", "namespace_mos_eisley_1_1_logic_1_1_tests.html", "namespace_mos_eisley_1_1_logic_1_1_tests" ],
    [ "AvgSkillResult", "class_mos_eisley_1_1_logic_1_1_avg_skill_result.html", "class_mos_eisley_1_1_logic_1_1_avg_skill_result" ],
    [ "AvgSpeedResult", "class_mos_eisley_1_1_logic_1_1_avg_speed_result.html", "class_mos_eisley_1_1_logic_1_1_avg_speed_result" ],
    [ "IPersonLogic", "interface_mos_eisley_1_1_logic_1_1_i_person_logic.html", "interface_mos_eisley_1_1_logic_1_1_i_person_logic" ],
    [ "ISpaceshipLogic", "interface_mos_eisley_1_1_logic_1_1_i_spaceship_logic.html", "interface_mos_eisley_1_1_logic_1_1_i_spaceship_logic" ],
    [ "PersonLogic", "class_mos_eisley_1_1_logic_1_1_person_logic.html", "class_mos_eisley_1_1_logic_1_1_person_logic" ],
    [ "SameEmployeeAndEmployerHomeworldResult", "class_mos_eisley_1_1_logic_1_1_same_employee_and_employer_homeworld_result.html", "class_mos_eisley_1_1_logic_1_1_same_employee_and_employer_homeworld_result" ],
    [ "SpaceshipLogic", "class_mos_eisley_1_1_logic_1_1_spaceship_logic.html", "class_mos_eisley_1_1_logic_1_1_spaceship_logic" ],
    [ "SumCreditsResult", "class_mos_eisley_1_1_logic_1_1_sum_credits_result.html", "class_mos_eisley_1_1_logic_1_1_sum_credits_result" ],
    [ "Top3FastestShipOwnerResult", "class_mos_eisley_1_1_logic_1_1_top3_fastest_ship_owner_result.html", "class_mos_eisley_1_1_logic_1_1_top3_fastest_ship_owner_result" ]
];