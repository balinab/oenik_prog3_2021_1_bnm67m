var interface_mos_eisley_1_1_logic_1_1_i_person_logic =
[
    [ "AddEmployee", "interface_mos_eisley_1_1_logic_1_1_i_person_logic.html#a116755263d6398b6b57dd582b57a8a3e", null ],
    [ "ChangeEmployeeName", "interface_mos_eisley_1_1_logic_1_1_i_person_logic.html#a01093b7004faae144c93af5a552a69b7", null ],
    [ "ChangeEmployerHomeworld", "interface_mos_eisley_1_1_logic_1_1_i_person_logic.html#a70ebb27393998e1da9088fa8c7d3fd0a", null ],
    [ "ChangeEmployerName", "interface_mos_eisley_1_1_logic_1_1_i_person_logic.html#a01ab64eb747f212574555ab50233b131", null ],
    [ "ChangeHomeworldEmployee", "interface_mos_eisley_1_1_logic_1_1_i_person_logic.html#ad37a2460f536833236868d7edd2c61f5", null ],
    [ "ChangeJobName", "interface_mos_eisley_1_1_logic_1_1_i_person_logic.html#a236642e8466e43f2b7f7a28251b670fe", null ],
    [ "ChangeSkillsEmployee", "interface_mos_eisley_1_1_logic_1_1_i_person_logic.html#a6eb9e012831331fb12897a875d03ea73", null ],
    [ "GetAllEmployees", "interface_mos_eisley_1_1_logic_1_1_i_person_logic.html#a99de82e3326a1901f75141f197052048", null ],
    [ "GetAllEmployers", "interface_mos_eisley_1_1_logic_1_1_i_person_logic.html#a654c984e4c001080dd21ea5ec6d8824a", null ],
    [ "GetAllJobs", "interface_mos_eisley_1_1_logic_1_1_i_person_logic.html#a9b0ef84ac7f71f3c16ceb90f533de5d2", null ],
    [ "GetEmployeeById", "interface_mos_eisley_1_1_logic_1_1_i_person_logic.html#a91614f664768326e3466dceed97e7e4e", null ],
    [ "GetEmployerById", "interface_mos_eisley_1_1_logic_1_1_i_person_logic.html#a5276c0bbc7bf8456704f58d6d987a941", null ],
    [ "GetJobAvg", "interface_mos_eisley_1_1_logic_1_1_i_person_logic.html#a30fdcaa0a543d637d6c72c9b6ca0bf16", null ],
    [ "GetJobById", "interface_mos_eisley_1_1_logic_1_1_i_person_logic.html#a75f141ef7a656f835f8a7db27ceb37a3", null ],
    [ "GetJobSumCredits", "interface_mos_eisley_1_1_logic_1_1_i_person_logic.html#a01d3c7686041fc0b65cc9526523c93d9", null ],
    [ "InsertEmployer", "interface_mos_eisley_1_1_logic_1_1_i_person_logic.html#a3367ac7678e24d854271af844fd8b32a", null ],
    [ "InsertJob", "interface_mos_eisley_1_1_logic_1_1_i_person_logic.html#ae22a8879c343d0b8dcac17e434d6d431", null ],
    [ "RemoveEmployee", "interface_mos_eisley_1_1_logic_1_1_i_person_logic.html#a0e76a0c611391e2495c8c65ce4138538", null ],
    [ "RemoveEmployer", "interface_mos_eisley_1_1_logic_1_1_i_person_logic.html#a066426d0d795fcb3d846204d9491b349", null ],
    [ "RemoveJob", "interface_mos_eisley_1_1_logic_1_1_i_person_logic.html#ad5e235f8b586eeb7d9a4b8eff66c3200", null ],
    [ "SameEmployeeAndEmployerHomeworld", "interface_mos_eisley_1_1_logic_1_1_i_person_logic.html#a3ed211d4e3aeea62f3f272c52764ae9f", null ]
];