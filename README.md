Tables: 
	Employer (Identichip (Primary key), Name, Homeworld, JobID (Foreign key), SpaceshipId (Foreign key))
	Employee (Identichip (Primary key), Name, Homeworld, Skills, JobID (Foreign key), SpaceshipId (Foreign key), EmployerId (Foreign key))
	Spaceship (SpaceshipId (Primary key), ShipName, Crew, Armament, Speed, Identichip (Foreign key))
	Job (JobId (Primary key), JobName)

Functions:
	Job list all / get a job by id / change the name of one job by id / add a job / remove a job by id
	Spaceship list all / get a spaceship by id / change the name of one spaceship by id / add a spaceship / remove a spaceship by id
	Employee list all / get a employee by id / change the name of one emyployee by id / add a employee / remove a employee by id
	Employer list all / get a employer by id / change the name of one emyployer by id / add a employer / remove a employer by id
	List the average skill points of the employees by their job 
	List the top 3 fastest spaceship of the employers
	List the employee and its employer if they are from the same homeworld