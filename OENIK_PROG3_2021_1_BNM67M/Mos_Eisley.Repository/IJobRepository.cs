﻿// <copyright file="IJobRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MosEisley.Repository
{
    using MosEisley.Data.Beta.Models;

    /// <summary>
    /// IJobRepository is a Interface.
    /// </summary>
    public interface IJobRepository : IRepo<Job>
    {
        /// <summary>
        /// Change Job Name.
        /// </summary>
        /// <param name="id">Identification key of the Job we want to change.</param>
        /// <param name="newjobname">The new name we want to change it.</param>
        void ChangeJobName(int id, string newjobname);
    }
}
