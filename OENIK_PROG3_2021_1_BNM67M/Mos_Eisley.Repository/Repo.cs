﻿// <copyright file="Repo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MosEisley.Repository
{
    using System;
    using System.Linq;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The main repository class.
    /// </summary>
    /// <typeparam name="T">Type of entity.</typeparam>
    public abstract class Repo<T> : IRepo<T>
        where T : class
    {
        /// <summary>
        /// dbcontext.
        /// </summary>
        protected DbContext ctx;

        /// <summary>
        /// Initializes a new instance of the <see cref="Repo{T}"/> class.
        /// </summary>
        /// <param name="ctx">Context of the entity.</param>
        protected Repo(DbContext ctx)
        {
            this.Contex = ctx;
        }

        /// <summary>
        /// Gets or sets.
        /// </summary>
        private protected DbContext Contex { get => this.ctx; set => this.ctx = value; }

        /// <inheritdoc/>
        public IQueryable<T> GetAll()
        {
            return this.Contex.Set<T>();
        }

        /// <inheritdoc/>
        public abstract T GetOne(int id);

        /// <inheritdoc/>
        public void Add(T entity)
        {
            this.Contex.Set<T>().Add(entity);
            this.Contex.SaveChanges();
        }

        /// <inheritdoc/>
        public bool Remove(int id)
        {
            var entity = this.Contex.Set<T>().Find(id);
            if (this.Contex.Set<T>().Contains(entity))
            {
                this.Contex.Set<T>().Remove(entity);
                this.Contex.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
