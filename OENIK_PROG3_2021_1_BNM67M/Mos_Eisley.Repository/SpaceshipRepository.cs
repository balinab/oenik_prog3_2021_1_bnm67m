﻿// <copyright file="SpaceshipRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MosEisley.Repository
{
    using System.Linq;
    using Microsoft.EntityFrameworkCore;
    using MosEisley.Data.Beta.Models;

    /// <summary>
    /// SpaceshipReposiroy class.
    /// </summary>
    public class SpaceshipRepository : Repo<Spaceship>, ISpaceshipRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SpaceshipRepository"/> class.
        /// </summary>
        /// <param name="ctx">Context of the entity.</param>
        public SpaceshipRepository(DbContext ctx)
            : base(ctx)
        {
            /*...*/
        }

        /// <inheritdoc/>
        public void ChangeArmament(int id, string newArmament)
        {
            var spaceship = this.GetOne(id);
            spaceship.Armament = newArmament;
            this.Contex.SaveChanges();
        }

        /// <inheritdoc/>
        public void ChangeCrew(int id, int newcrewnumber)
        {
            var spaceship = this.GetOne(id);
            spaceship.Crew = newcrewnumber;
            this.Contex.SaveChanges();
        }

        /// <inheritdoc/>
        public void ChangeMaximumSpeed(int id, int newmaxiumumspeed)
        {
            var spaceship = this.GetOne(id);
            spaceship.MaxSpeed = newmaxiumumspeed;
            this.Contex.SaveChanges();
        }

        /// <inheritdoc/>
        public void ChangeSpaceshipName(int id, string newname)
        {
            var spaceship = this.GetOne(id);
            spaceship.SpaceshipName = newname;
            this.Contex.SaveChanges();
        }

        /// <inheritdoc/>
        public override Spaceship GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.SpaceshipId == id);
        }
    }
}
