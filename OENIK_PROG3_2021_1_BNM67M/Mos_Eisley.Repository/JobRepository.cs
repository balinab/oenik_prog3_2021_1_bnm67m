﻿// <copyright file="JobRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MosEisley.Repository
{
    using System.Linq;
    using Microsoft.EntityFrameworkCore;
    using MosEisley.Data.Beta.Models;

    /// <summary>
    /// Job repository class.
    /// </summary>
    public class JobRepository : Repo<Job>, IJobRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="JobRepository"/> class.
        /// </summary>
        /// <param name="ctx">Context of the entity.</param>
        public JobRepository(DbContext ctx)
            : base(ctx)
        {
            /*...*/
        }

        /// <inheritdoc/>
        public void ChangeJobName(int id, string newjobname)
        {
            var job = this.GetOne(id);
            job.JobName = newjobname;
            this.Contex.SaveChanges();
        }

        /// <inheritdoc/>
        public override Job GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.JobId == id);
        }
    }
}
