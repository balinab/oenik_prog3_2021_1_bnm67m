﻿// <copyright file="EmployeeRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MosEisley.Repository
{
    using System.Linq;
    using Microsoft.EntityFrameworkCore;
    using MosEisley.Data.Beta.Models;

    /// <summary>
    /// EmployeeRepository contians the entity specific cruds and inherit others.
    /// </summary>
    public class EmployeeRepository : Repo<Employee>, IEmployeeRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeRepository"/> class.
        /// </summary>
        /// <param name="ctx">The contexts of the actual entity.</param>
        public EmployeeRepository(DbContext ctx)
            : base(ctx)
        {
            /*...*/
        }

        /// <inheritdoc/>
        public void ChangeHomeworld(int id, string newhomeworld)
        {
            var employee = this.GetOne(id);
            employee.Homeworld = newhomeworld;
            this.Contex.SaveChanges();
        }

        /// <inheritdoc/>
        public void ChangeName(int id, string newname)
        {
            var employee = this.GetOne(id);
            employee.Name = newname;
            this.Contex.SaveChanges();
        }

        /// <inheritdoc/>
        public void ChangeSkills(int id, int newskills)
        {
            var employee = this.GetOne(id);
            employee.Skills = newskills;
            this.Contex.SaveChanges();
        }

        /// <inheritdoc/>
        public override Employee GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.Identichip == id);
        }
    }
}
