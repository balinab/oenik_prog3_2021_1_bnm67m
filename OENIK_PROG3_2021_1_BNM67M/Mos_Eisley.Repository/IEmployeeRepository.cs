﻿// <copyright file="IEmployeeRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MosEisley.Repository
{
    using MosEisley.Data.Beta.Models;

    /// <summary>
    /// IEmpliyeeRepository is a Interface.
    /// </summary>
    public interface IEmployeeRepository : IRepo<Employee>
    {
        /// <summary>
        /// Change Employee Name.
        /// </summary>
        /// <param name="id">Identification key of the Employee we want to change.</param>
        /// <param name="newname">The new name we want to change it.</param>
        void ChangeName(int id, string newname);

        /// <summary>
        /// Change Employee Homeworld.
        /// </summary>
        /// <param name="id">Identification key of the Employee we want to change.</param>
        /// <param name="newhomeworld">The new homeworld we want to change it.</param>
        void ChangeHomeworld(int id, string newhomeworld);

        /// <summary>
        /// Change Employee Skills.
        /// </summary>
        /// <param name="id">Identification key of the Employee we want to change.</param>
        /// <param name="newskills">The new skills we want to change it.</param>
        void ChangeSkills(int id, int newskills);
    }
}
