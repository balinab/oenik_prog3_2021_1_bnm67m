﻿// <copyright file="IEmployerRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MosEisley.Repository
{
    using MosEisley.Data.Beta.Models;

    /// <summary>
    /// IEmployerRepository is a Interface.
    /// </summary>
    public interface IEmployerRepository : IRepo<Employer>
    {
        /// <summary>
        /// Change Employer Name.
        /// </summary>
        /// <param name="id">Identification key of the Employer we want to change.</param>
        /// <param name="newname">The new name we want to change it.</param>
        void ChangeName(int id, string newname);

        /// <summary>
        /// Change Employee Name.
        /// </summary>
        /// <param name="id">Identification key of the Employer we want to change.</param>
        /// <param name="newhomeworld">The new homeworld we want to change it.</param>
        void ChangeHomeworld(int id, string newhomeworld);
    }
}
