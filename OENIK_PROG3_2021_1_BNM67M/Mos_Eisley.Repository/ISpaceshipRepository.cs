﻿// <copyright file="ISpaceshipRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MosEisley.Repository
{
    using MosEisley.Data.Beta.Models;

    /// <summary>
    /// The interface of Spaceship Repository.
    /// </summary>
    public interface ISpaceshipRepository : IRepo<Spaceship>
    {
        /// <summary>
        /// Change Spaceship Name.
        /// </summary>
        /// <param name="id">Identification key of the Spaceship we want to change. </param>
        /// <param name="newname">The new name we want to change it.</param>
        void ChangeSpaceshipName(int id, string newname);

        /// <summary>
        /// Change Spaceship Crew.
        /// </summary>
        /// <param name="id">Identification key of the Spaceship we want to change. </param>
        /// <param name="newcrewnumber">The new number of the crew we want to change it.</param>
        void ChangeCrew(int id, int newcrewnumber);

        /// <summary>
        /// Change Spaceship Armament.
        /// </summary>
        /// <param name="id">Identification key of the Spaceship we want to change. </param>
        /// <param name="newArmament">The new number of the crew we want to change it.</param>
        void ChangeArmament(int id, string newArmament);

        /// <summary>
        /// Change Maximum speed of the spaceship.
        /// </summary>
        /// <param name="id">Identification key of the Spaceship we want to change.</param>
        /// <param name="newmaxiumumspeed">The nemw maximum speed of the spaceship.</param>
        void ChangeMaximumSpeed(int id, int newmaxiumumspeed);
    }
}
