﻿// <copyright file="EmployerRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MosEisley.Repository
{
    using System.Linq;
    using Microsoft.EntityFrameworkCore;
    using MosEisley.Data.Beta.Models;

    /// <summary>
    /// EmployeRRepository contians the entity specific cruds and inherit others.
    /// </summary>
    public class EmployerRepository : Repo<Employer>, IEmployerRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmployerRepository"/> class.
        /// </summary>
        /// <param name="ctx">The contexts of the actual entity.</param>
        public EmployerRepository(DbContext ctx)
            : base(ctx)
        {
            /*...*/
        }

        /// <inheritdoc/>
        public void ChangeHomeworld(int id, string newhomeworld)
        {
            var employer = this.GetOne(id);
            employer.Homeworld = newhomeworld;
            this.Contex.SaveChanges();
        }

        /// <inheritdoc/>
        public void ChangeName(int id, string newname)
        {
            var employer = this.GetOne(id);
            employer.Name = newname;
            this.Contex.SaveChanges();
        }

        /// <inheritdoc/>
        public override Employer GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.Identichip == id);
        }
    }
}
