﻿// <copyright file="IRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MosEisley.Repository
{
    using System.Linq;

    /// <summary>
    /// The main interface of repository.
    /// </summary>
    /// <typeparam name="T">type of entity.</typeparam>
    public interface IRepo<T>
        where T : class
    {
        /// <summary>
        /// Giveback the entity by id.
        /// </summary>
        /// <param name="id">Id of the entity.</param>
        /// <returns>List.</returns>
        T GetOne(int id);

        /// <summary>
        /// Get all of an entity type.
        /// </summary>
        /// <returns>An IQueryable of all entity of type.</returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Insert one entity.
        /// </summary>
        /// <param name="entity">entity.</param>
        void Add(T entity);

        /// <summary>
        /// Remove an entity by id.
        /// </summary>
        /// <param name="id">Id of an entity.</param>
        /// <returns>If its done.</returns>
        bool Remove(int id);
    }
}
