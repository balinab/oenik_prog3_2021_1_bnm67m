﻿// <copyright file="SpaceshipLogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MosEisley.Logic.Tests
{
    using System.Collections.Generic;
    using System.Linq;
    using Moq;
    using MosEisley.Data.Beta.Models;
    using MosEisley.Repository;
    using NUnit.Framework;

    /// <summary>
    /// This class contains tests for SpaceshipLogic.
    /// </summary>
    [TestFixture]
    public class SpaceshipLogicTests
    {
        private Mock<ISpaceshipRepository> mockedSpaceshipRepo;
        private Mock<IEmployeeRepository> mockedEmployeeRepo;
        private SpaceshipLogic spaceshipLogic;

        /// <summary>
        /// Sets up mocked repository and logic.
        /// </summary>
        [SetUp]
        public void SetupTest()
        {
            this.mockedSpaceshipRepo = new Mock<ISpaceshipRepository>(MockBehavior.Loose);
            this.mockedEmployeeRepo = new Mock<IEmployeeRepository>(MockBehavior.Loose);
            this.spaceshipLogic = new SpaceshipLogic(this.mockedSpaceshipRepo.Object, this.mockedEmployeeRepo.Object);
        }

        /// <summary>
        /// Tests the ChangeSpaceshipName method.
        /// </summary>
        [Test]
        public void TestChangeSpaceshipName()
        {
            // Arrange
            this.mockedSpaceshipRepo.Setup(repo => repo.ChangeSpaceshipName(It.IsAny<int>(), It.IsAny<string>()));

            // Act
            this.spaceshipLogic.ChangeSpaceshipName(1, "Test");

            // Assert
            this.mockedSpaceshipRepo.Verify(repo => repo.ChangeSpaceshipName(1, "Test"), Times.Once);
            this.mockedSpaceshipRepo.Verify(repo => repo.ChangeSpaceshipName(2, "asd"), Times.Never);
            this.mockedSpaceshipRepo.Verify(repo => repo.GetOne(It.IsAny<int>()), Times.Never);
        }

        /// <summary>
        /// Tests the GetSpaceshipById method.
        /// </summary>
        [Test]
        public void TestGetSpaceshipById()
        {
            // Act
            var result = this.spaceshipLogic.GetSpaceshipById(2);

            // Assert
            this.mockedSpaceshipRepo.Verify(repo => repo.GetOne(It.IsAny<int>()), Times.Once);
            this.mockedSpaceshipRepo.Verify(repo => repo.GetAll(), Times.Never);
        }

        /// <summary>
        /// Test the GetTop3FastestShipOwnerEmployees.
        /// </summary>
        [Test]
        public void TestGetTop3FastestShipOwnerEmployees()
        {
            // Arrange
            Spaceship millenium = new () { SpaceshipId = 1, SpaceshipName = "Millennium Falcon", MaxSpeed = 10 };
            Spaceship ship1 = new () { SpaceshipId = 2, SpaceshipName = "asd", MaxSpeed = 7 };
            Spaceship ship2 = new () { SpaceshipId = 3, SpaceshipName = "das", MaxSpeed = 8 };
            Spaceship ship3 = new () { SpaceshipId = 4, SpaceshipName = "sdfd", MaxSpeed = 8 };
            Employee hansolo = new () { Name = "Han Solo", SpaceshipId = 1, Spaceship = millenium };
            Employee greedo = new () { Name = "Greedo", SpaceshipId = 2, Spaceship = ship1 };
            Employee sanyi = new () { Name = "Sanyi", SpaceshipId = 3, Spaceship = ship2 };
            Employee lajos = new () { Name = "Lajos", SpaceshipId = 4, Spaceship = ship3 };
            List<Employee> employees = new () { greedo, hansolo, sanyi, lajos };
            List<Spaceship> spaceships = new () { millenium, ship1, ship2, ship3 };

            List<Top3FastestShipOwnerResult> expectedResult = new ()
            {
                new Top3FastestShipOwnerResult() { OwnerName = "Han Solo", ShipName = "Millennium Falcon", MaximumSpeed = 10 },
                new Top3FastestShipOwnerResult() { OwnerName = "Lajos", ShipName = "sdfd", MaximumSpeed = 8 },
                new Top3FastestShipOwnerResult() { OwnerName = "Sanyi", ShipName = "das", MaximumSpeed = 8 },
            };

            this.mockedEmployeeRepo.Setup(repo => repo.GetAll()).Returns(employees.AsQueryable());
            this.mockedSpaceshipRepo.Setup(repo => repo.GetAll()).Returns(spaceships.AsQueryable());

            // Act
            var actualTop3 = this.spaceshipLogic.GetTop3FastestShipOwnerEmployees();

            // Assert
            Assert.That(actualTop3, Is.EquivalentTo(expectedResult));
            this.mockedEmployeeRepo.Verify(repo => repo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Test the GetSpeedAvg.
        /// </summary>
        [Test]
        public void TestGetSpeedAvg()
        {
            // Arrange
            Employee sanyi = new () { SpaceshipId = 66, Homeworld = "Planet1" };
            Employee musican = new () { SpaceshipId = 33, Homeworld = "Planet2" };
            List<Employee> emps = new () { sanyi, musican };
            List<Spaceship> ships = new ()
            {
                new Spaceship() { SpaceshipId = 66, MaxSpeed = 100 },
                new Spaceship() { SpaceshipId = 66, MaxSpeed = 150 },
                new Spaceship() { SpaceshipId = 33, MaxSpeed = 200 },
            };

            List<AvgSpeedResult> expectedAvgs = new ()
            {
                new AvgSpeedResult() { HomeworldName = "Planet1", AverageSpeed = 125 },
                new AvgSpeedResult() { HomeworldName = "Planet2", AverageSpeed = 200 },
            };

            this.mockedSpaceshipRepo.Setup(repo => repo.GetAll()).Returns(ships.AsQueryable());
            this.mockedEmployeeRepo.Setup(repo => repo.GetAll()).Returns(emps.AsQueryable());

            // Act
            var actualAvgs = this.spaceshipLogic.GetSpeedAvg();

            // Assert
            Assert.That(actualAvgs, Is.EquivalentTo(expectedAvgs));
            this.mockedSpaceshipRepo.Verify(repo => repo.GetAll(), Times.Once);
            this.mockedEmployeeRepo.Verify(repo => repo.GetAll(), Times.Once);
        }
    }
}
