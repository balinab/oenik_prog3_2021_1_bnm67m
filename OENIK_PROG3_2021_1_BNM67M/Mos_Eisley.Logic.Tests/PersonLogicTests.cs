﻿// <copyright file="PersonLogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MosEisley.Logic.Tests
{
    using System.Collections.Generic;
    using System.Linq;
    using Moq;
    using MosEisley.Data.Beta.Models;
    using MosEisley.Repository;
    using NUnit.Framework;

    /// <summary>
    /// This class contains tests for PersonLogic.
    /// </summary>
    [TestFixture]
    public class PersonLogicTests
    {
        private Mock<IEmployeeRepository> mockedEmployeeRepo;
        private Mock<IEmployerRepository> mockedEmployerRepo;
        private Mock<IJobRepository> mockedJobRepo;
        private PersonLogic personLogic;

        /// <summary>
        /// Sets up mocked repository and logic.
        /// </summary>
        [SetUp]
        public void SetupTest()
        {
            this.mockedEmployeeRepo = new Mock<IEmployeeRepository>(MockBehavior.Loose);
            this.mockedEmployerRepo = new Mock<IEmployerRepository>(MockBehavior.Loose);
            this.mockedJobRepo = new Mock<IJobRepository>(MockBehavior.Loose);
            this.personLogic = new PersonLogic(this.mockedEmployeeRepo.Object, this.mockedJobRepo.Object, this.mockedEmployerRepo.Object);
        }

        /// <summary>
        /// Test the ChangeNameEmployee method.
        /// </summary>
        [Test]
        public void TestChangeNameEmployee()
        {
            // Arrange
            this.mockedEmployeeRepo.Setup(repo => repo.ChangeName(It.IsAny<int>(), It.IsAny<string>()));

            // Act
            this.personLogic.ChangeEmployeeName(1, "Test");

            // Assert
            this.mockedEmployeeRepo.Verify(repo => repo.ChangeName(1, "Test"), Times.Once);
            this.mockedEmployeeRepo.Verify(repo => repo.ChangeName(2, "asd"), Times.Never);
            this.mockedEmployeeRepo.Verify(repo => repo.GetOne(It.IsAny<int>()), Times.Never);
        }

        /// <summary>
        /// Test the ChangeNameEmployer method.
        /// </summary>
        [Test]
        public void TestChangeNameEmployer()
        {
            // Arrange
            this.mockedEmployerRepo.Setup(repo => repo.ChangeName(It.IsAny<int>(), It.IsAny<string>()));

            // Act
            this.personLogic.ChangeEmployerName(1, "Test");

            // Assert
            this.mockedEmployerRepo.Verify(repo => repo.ChangeName(1, "Test"), Times.Once);
            this.mockedEmployerRepo.Verify(repo => repo.ChangeName(2, "asd"), Times.Never);
            this.mockedEmployerRepo.Verify(repo => repo.GetOne(It.IsAny<int>()), Times.Never);
        }

        /// <summary>
        /// Test the ChangeJobName method.
        /// </summary>
        [Test]
        public void TestChangeJobName()
        {
            // Arrange
            this.mockedJobRepo.Setup(repo => repo.ChangeJobName(It.IsAny<int>(), It.IsAny<string>()));

            // Act
            this.personLogic.ChangeJobName(1, "Test");

            // Assert
            this.mockedJobRepo.Verify(repo => repo.ChangeJobName(1, "Test"), Times.Once);
            this.mockedJobRepo.Verify(repo => repo.ChangeJobName(2, "asd"), Times.Never);
            this.mockedJobRepo.Verify(repo => repo.GetOne(It.IsAny<int>()), Times.Never);
        }

        /// <summary>
        /// Test the GetEmployeeById method.
        /// </summary>
        [Test]
        public void TestGetEmployeeById()
        {
            // Act
            var result = this.personLogic.GetEmployeeById(2);

            // Assert
            this.mockedEmployeeRepo.Verify(repo => repo.GetOne(It.IsAny<int>()), Times.Once);
            this.mockedEmployeeRepo.Verify(repo => repo.GetAll(), Times.Never);
        }

        /// <summary>
        /// Test the GetEmployerById method.
        /// </summary>
        [Test]
        public void TestGetEmployerById()
        {
            // Act
            var result = this.personLogic.GetEmployerById(2);

            // Assert
            this.mockedEmployerRepo.Verify(repo => repo.GetOne(It.IsAny<int>()), Times.Once);
            this.mockedEmployerRepo.Verify(repo => repo.GetAll(), Times.Never);
        }

        /// <summary>
        /// Test the GetEmployerById method.
        /// </summary>
        [Test]
        public void TestGetJobById()
        {
            // Act
            var result = this.personLogic.GetJobById(2);

            // Assert
            this.mockedJobRepo.Verify(repo => repo.GetOne(It.IsAny<int>()), Times.Once);
            this.mockedJobRepo.Verify(repo => repo.GetAll(), Times.Never);
        }

        /// <summary>
        /// Test the GetJobAvg.
        /// </summary>
        [Test]
        public void TestGetJobAvg()
        {
            // Arrange
            Job bountyHunter = new () { JobId = 66, JobName = "Bounty Hunter" };
            Job musican = new () { JobId = 33, JobName = "Musican" };
            List<Job> jobs = new () { bountyHunter, musican };
            List<Employee> employees = new ()
            {
                new Employee() { JobId = 66, Skills = 9 },
                new Employee() { JobId = 66, Skills = 7 },
                new Employee() { JobId = 33, Skills = 10 },
            };

            List<AvgSkillResult> expectedAvgs = new ()
            {
                new AvgSkillResult() { JobName = "Bounty Hunter", AverageSkills = 8 },
                new AvgSkillResult() { JobName = "Musican", AverageSkills = 10 },
            };

            this.mockedEmployeeRepo.Setup(repo => repo.GetAll()).Returns(employees.AsQueryable());
            this.mockedJobRepo.Setup(repo => repo.GetAll()).Returns(jobs.AsQueryable());

            // Act
            var actualAvgs = this.personLogic.GetJobAvg();

            // Assert
            Assert.That(actualAvgs, Is.EquivalentTo(expectedAvgs));
            this.mockedEmployeeRepo.Verify(repo => repo.GetAll(), Times.Once);
            this.mockedJobRepo.Verify(repo => repo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Test the GetJobSumCredits.
        /// </summary>
        [Test]
        public void TestGetJobSumCredits()
        {
            // Arrange
            Job bountyHunter = new () { JobId = 66, JobName = "Bounty Hunter" };
            Job musican = new () { JobId = 33, JobName = "Musican" };
            List<Job> jobs = new () { bountyHunter, musican };
            List<Employer> employers = new ()
            {
                new Employer() { JobId = 66, CreditsOwn = 657 },
                new Employer() { JobId = 66, CreditsOwn = 458 },
                new Employer() { JobId = 33, CreditsOwn = 666 },
            };

            List<SumCreditsResult> expectedSum = new ()
            {
                new SumCreditsResult() { JobName = "Bounty Hunter", SumCredits = 1115 },
                new SumCreditsResult() { JobName = "Musican", SumCredits = 666 },
            };

            this.mockedEmployerRepo.Setup(repo => repo.GetAll()).Returns(employers.AsQueryable());
            this.mockedJobRepo.Setup(repo => repo.GetAll()).Returns(jobs.AsQueryable());

            // Act
            var actualSum = this.personLogic.GetJobSumCredits();

            // Assert
            Assert.That(actualSum, Is.EquivalentTo(expectedSum));
            this.mockedEmployerRepo.Verify(repo => repo.GetAll(), Times.Once);
            this.mockedJobRepo.Verify(repo => repo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Test the SameEmployeeAndEmployerHomeworld.
        /// </summary>
        [Test]
        public void TestSameEmployeeAndEmployerHomeworld()
        {
            // Arrange
            Employer skywalker = new () { Name = "Luke Skywalker", Homeworld = "Tattione" };
            Employer obiwan = new () { Name = "Obi Wan", Homeworld = "Stewjon" };
            Employee greedo = new () { Name = "Greedo", Homeworld = "Tattione", Employer = skywalker };
            Employee hansolo = new () { Name = "Han Solo", Homeworld = "Corellia", Employer = obiwan };
            List<Employee> employees = new () { greedo, hansolo };

            List<SameEmployeeAndEmployerHomeworldResult> expectedRes = new ()
            {
                new SameEmployeeAndEmployerHomeworldResult()
                {
                    EmployeeName = "Greedo",
                    EmployerName = "Luke Skywalker",
                    SameHomeworld = "Tattione",
                },
            };

            this.mockedEmployeeRepo.Setup(repo => repo.GetAll()).Returns(employees.AsQueryable());

            // Act
            var employerAndEmployeeHomeworldResult = this.personLogic.SameEmployeeAndEmployerHomeworld();

            // Assert
            Assert.That(employerAndEmployeeHomeworldResult, Is.EquivalentTo(expectedRes));
            this.mockedEmployeeRepo.Verify(repo => repo.GetAll(), Times.Once);
        }
    }
}
