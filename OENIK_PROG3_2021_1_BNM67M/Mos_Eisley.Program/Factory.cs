﻿// <copyright file="Factory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MosEisley.Program
{
    using MosEisley.Data.Beta.Models;
    using MosEisley.Logic;
    using MosEisley.Repository;

    /// <summary>
    /// Factory class collect the initalasions.
    /// </summary>
    public class Factory
    {
        private readonly JobRepository jobRepo;
        private readonly SpaceshipRepository spaceshipRepo;
        private readonly EmployerRepository employerRepo;
        private readonly EmployeeRepository employeeRepo;

        /// <summary>
        /// Person logic added.
        /// </summary>
        private PersonLogic personLogic;

        /// <summary>
        /// Spaceship logic added.
        /// </summary>
        private SpaceshipLogic spaceshipLogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="Factory"/> class.
        /// </summary>
        public Factory()
        {
            ModelContext ctx = new ModelContext();

            this.jobRepo = new JobRepository(ctx);
            this.spaceshipRepo = new SpaceshipRepository(ctx);
            this.employeeRepo = new EmployeeRepository(ctx);
            this.employerRepo = new EmployerRepository(ctx);

            this.PersonLogic = new PersonLogic(this.employeeRepo, this.jobRepo, this.employerRepo);
            this.SpaceshipLogic = new SpaceshipLogic(this.spaceshipRepo, this.employeeRepo);
        }

        /// <summary>
        /// Gets or sets of PersonLogic.
        /// </summary>
        public PersonLogic PersonLogic { get => this.personLogic; set => this.personLogic = value; }

        /// <summary>
        /// Gets or sets of SpacehipLogic.
        /// </summary>
        public SpaceshipLogic SpaceshipLogic { get => this.spaceshipLogic; set => this.spaceshipLogic = value; }
    }
}
