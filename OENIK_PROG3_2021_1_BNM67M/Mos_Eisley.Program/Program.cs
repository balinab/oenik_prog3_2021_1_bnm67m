﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MosEisley.Program
{
    using System;
    using System.Linq;
    using ConsoleTools;
    using MosEisley.Data.Beta.Models;
    using MosEisley.Logic;

    /// <summary>
    /// Program contains the program menu and its setup.
    /// </summary>
    internal class Program
    {
        private static void Main()
        {
            using ModelContext ctx = new ();
            Factory factory = new Factory();

            var menu = new ConsoleMenu()
                .Add(">> LIST ALL JOBS <<", () => ListAllJobs(factory.PersonLogic))
                .Add("\t>> GET BY ID", () => GetJobById(factory.PersonLogic))
                .Add("\t>> CHANGE NAME", () => ChangeJobName(factory.PersonLogic))
                .Add("\t>> ADD JOB", () => AddJob(factory.PersonLogic))
                .Add("\t>> REMOVE JOB", () => RemoveJob(factory.PersonLogic))
                .Add(">> LIST ALL SPACESHIPS <<", () => ListAllSpaceships(factory.SpaceshipLogic))
                .Add("\t>> GET BY ID", () => GetSpaceshipById(factory.SpaceshipLogic))
                .Add("\t>> CHANGE NAME", () => ChangeSpaceshipName(factory.SpaceshipLogic))
                .Add("\t>> ADD SPACESHIP", () => AddSpaceship(factory.SpaceshipLogic))
                .Add("\t>> REMOVE SPACESHIP", () => RemoveSpaceship(factory.SpaceshipLogic))
                .Add(">> LIST ALL EMPLOYERS <<", () => ListAllEmployers(factory.PersonLogic))
                .Add("\t>> GET BY ID", () => GetEmployerById(factory.PersonLogic))
                .Add("\t>> CHANGE NAME", () => ChangeEmployerName(factory.PersonLogic))
                .Add("\t>> ADD EMPLOYER", () => AddEmployer(factory.PersonLogic))
                .Add("\t>> REMOVE EMPLOYER", () => RemoveEmployer(factory.PersonLogic))
                .Add(">> LIST ALL EMPLOYEES <<", () => ListAllEmployees(factory.PersonLogic))
                .Add("\t>> GET BY ID", () => GetEmployeeById(factory.PersonLogic))
                .Add("\t>> CHANGE NAME", () => ChangeEmployeeName(factory.PersonLogic))
                .Add("\t>> ADD EMPLOYEE", () => AddEmployee(factory.PersonLogic))
                .Add("\t>> REMOVE EMPLOYEE", () => RemoveEmployee(factory.PersonLogic))
                .Add("AVERAGE SKILLS BY JOB TYPES", () => ListJobAvgSkills(factory.PersonLogic))
                .Add("PEOPLE FROM THE SAME HOMEWORLD", () => ListSameHomeworld(factory.PersonLogic))
                .Add("THE TOP3 FASTEST SPACESHIP OF EMPLOYEES", () => ListTop3FastestShip(factory.SpaceshipLogic))
                .Add("ALL CREDITS OF EMPLOYERS FROM THE SAME JOB THEY ARE LOOKING FOR", () => ListJobSumCredits(factory.PersonLogic))
                .Add("AVERAGE SPEED OF EMPLOYEES SHIPS BY HOMEWORLDS", () => ListAvgSpeed(factory.SpaceshipLogic))
                .Add("AVERAGE SKILLS BY JOB TYPES ASYNC VERSION", () => ListJobAvgSkillsAsync(factory.PersonLogic))
                .Add("PEOPLE FROM THE SAME HOMEWORLD ASYNC VERSION", () => ListSameHomeworldAsync(factory.PersonLogic))
                .Add("THE TOP3 FASTEST SPACESHIP OF EMPLOYEES ASYNC VERSION", () => ListTop3FastestShipAsync(factory.SpaceshipLogic))
                .Add("ALL CREDITS OF EMPLOYERS FROM THE SAME JOB THEY ARE LOOKING FOR ASYNC VERSION", () => ListJobSumCreditsAsync(factory.PersonLogic))
                .Add("AVERAGE SPEED OF EMPLOYEES SHIPS BY HOMEWORLDS ASYNC VERSION", () => ListAvgSpeedAsync(factory.SpaceshipLogic))
                .Add(">> EXIT", ConsoleMenu.Close);

            menu.Show();
        }

        private static void ListAvgSpeed(SpaceshipLogic spaceshipLogic)
        {
            foreach (var item in spaceshipLogic.GetSpeedAvg())
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("\nPress any key to return");
            Console.ReadLine();
        }

        private static void ListAvgSpeedAsync(SpaceshipLogic spaceshipLogic)
        {
            var result = spaceshipLogic.GetSpeedAvgAsync().Result;
            foreach (var item in result)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("\nPress any key to return");
            Console.ReadLine();
        }

        private static void ListJobSumCredits(PersonLogic personLogic)
        {
            foreach (var item in personLogic.GetJobSumCredits())
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("\nPress any key to return");
            Console.ReadLine();
        }

        private static void ListJobSumCreditsAsync(PersonLogic personLogic)
        {
            var result = personLogic.GetJobSumCreditsAsync().Result;
            foreach (var item in result)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("\nPress any key to return");
            Console.ReadLine();
        }

        private static void ListTop3FastestShip(SpaceshipLogic spaceshipLogic)
        {
            foreach (var item in spaceshipLogic.GetTop3FastestShipOwnerEmployees())
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("\nPress any key to return");
            Console.ReadLine();
        }

        private static void ListTop3FastestShipAsync(SpaceshipLogic spaceshipLogic)
        {
            var result = spaceshipLogic.GetTop3FastestShipOwnerEmployeesAsync().Result;
            foreach (var item in result)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("\nPress any key to return");
            Console.ReadLine();
        }

        private static void ListSameHomeworld(PersonLogic personLogic)
        {
            foreach (var item in personLogic.SameEmployeeAndEmployerHomeworld())
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("\nPress any key to return");
            Console.ReadLine();
        }

        private static void ListSameHomeworldAsync(PersonLogic personLogic)
        {
            var result = personLogic.SameEmployeeAndEmployerHomeworldAsync().Result;
            foreach (var item in result)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("\nPress any key to return");
            Console.ReadLine();
        }

        private static void ListJobAvgSkills(PersonLogic personLogic)
        {
            foreach (var item in personLogic.GetJobAvg())
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("\nPress any key to return");
            Console.ReadLine();
        }

        private static void ListJobAvgSkillsAsync(PersonLogic personLogic)
        {
            var result = personLogic.GetJobAvgAsync().Result;
            foreach (var item in result)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("\nPress any key to return");
            Console.ReadLine();
        }

        private static void ListAllJobs(PersonLogic personLogic)
        {
            Console.WriteLine("\n:: ALL JOBS ::\n");
            personLogic.GetAllJobs()
                .ToList()
                .ForEach(x => Console.WriteLine(x.MainData));

            Console.WriteLine("\nPress any key to return");
            Console.ReadLine();
        }

        private static void ListAllSpaceships(SpaceshipLogic spaceshipLogic)
        {
            Console.WriteLine("\n:: ALL SPACESHIPS ::\n");
            spaceshipLogic.GetAllSpaceships()
                .ToList()
                .ForEach(x => Console.WriteLine(x.MainData));

            Console.WriteLine("\nPress any key to return");
            Console.ReadLine();
        }

        private static void ListAllEmployers(PersonLogic personLogic)
        {
            Console.WriteLine("\n:: ALL EMPLOYERS ::\n");
            personLogic.GetAllEmployers()
                .ToList()
                .ForEach(x => Console.WriteLine(x.MainData));

            Console.WriteLine("\nPress any key to return");
            Console.ReadLine();
        }

        private static void ListAllEmployees(PersonLogic logic)
        {
            Console.WriteLine("\n:: ALL EMPLOYERS ::\n");
            logic.GetAllEmployees()
                .ToList()
                .ForEach(x => Console.WriteLine(x.MainData));

            Console.WriteLine("\nPress any key to return");
            Console.ReadLine();
        }

        private static void GetJobById(PersonLogic logic)
        {
            Console.Write("ENTER ID HERE: ");
            int id;
            id = int.TryParse(Console.ReadLine(), out id) ? id : -1;
            if (id == -1)
            {
                Console.WriteLine("Not a valid Id");
            }
            else
            {
                var q = logic.GetJobById(id);

                Console.WriteLine("\n:: SELECTED JOB ::\n");
                Console.WriteLine(q.MainData);
            }

            Console.WriteLine("\nPress any key to return");
            Console.ReadLine();
        }

        private static void GetSpaceshipById(SpaceshipLogic spaceshipLogic)
        {
            Console.Write("ENTER ID HERE: ");
            int id;
            id = int.TryParse(Console.ReadLine(), out id) ? id : -1;
            if (id == -1)
            {
                Console.WriteLine("Not a valid Id");
            }
            else
            {
                var q = spaceshipLogic.GetSpaceshipById(id);

                Console.WriteLine("\n:: SELECTED SPACESHIP ::\n");
                Console.WriteLine(q.MainData);
            }

            Console.WriteLine("\nPress any key to return");
            Console.ReadLine();
        }

        private static void GetEmployerById(PersonLogic logic)
        {
            Console.Write("ENTER ID HERE: ");
            int id;
            id = int.TryParse(Console.ReadLine(), out id) ? id : -1;
            if (id == -1)
            {
                Console.WriteLine("Not a valid Id");
            }
            else
            {
                var q = logic.GetEmployerById(id);

                Console.WriteLine("\n:: SELECTED EMPLOYER ::\n");
                Console.WriteLine(q.MainData);
            }

            Console.WriteLine("\nPress any key to return");
            Console.ReadLine();
        }

        private static void GetEmployeeById(PersonLogic logic)
        {
            Console.Write("ENTER ID HERE: ");
            int id;
            id = int.TryParse(Console.ReadLine(), out id) ? id : -1;
            if (id == -1)
            {
                Console.WriteLine("Not a valid Id");
            }
            else
            {
                var q = logic.GetEmployeeById(id);

                Console.WriteLine("\n:: SELECTED EMPLOYEE ::\n");
                Console.WriteLine(q.MainData);
            }

            Console.WriteLine("\nPress any key to return");
            Console.ReadLine();
        }

        private static void ChangeSpaceshipName(SpaceshipLogic spaceshipLogic)
        {
            Console.Write("ENTER ID HERE: ");
            int id;
            id = int.TryParse(Console.ReadLine(), out id) ? id : -1;
            if (id == -1)
            {
                Console.WriteLine("Not a valid Id");
            }
            else
            {
                Console.Write("ENTER NEW SPACESHIP NAME HERE: ");
                string newSpaceshipName = Console.ReadLine();

                spaceshipLogic.ChangeSpaceshipName(id, newSpaceshipName);

                var q = spaceshipLogic.GetSpaceshipById(id);

                Console.WriteLine("\n:: NEW SPACESHIP NAME ::\n");
                Console.WriteLine(q.SpaceshipName);
            }

            Console.WriteLine("\nPress any key to return");
            Console.ReadLine();
        }

        private static void ChangeEmployerName(PersonLogic employerLogic)
        {
            Console.Write("ENTER ID HERE: ");
            int id;
            id = int.TryParse(Console.ReadLine(), out id) ? id : -1;
            if (id == -1)
            {
                Console.WriteLine("Not a valid Id");
            }
            else
            {
                Console.Write("ENTER NEW EMPLOYEE NAME HERE: ");
                string newEmployerName = Console.ReadLine();

                employerLogic.ChangeEmployerName(id, newEmployerName);

                var q = employerLogic.GetEmployerById(id);

                Console.WriteLine("\n:: NEW EMPLOYER NAME ::\n");
                Console.WriteLine(q.Name);
            }

            Console.WriteLine("\nPress any key to return");
            Console.ReadLine();
        }

        private static void ChangeEmployeeName(PersonLogic employeeLogic)
        {
            Console.Write("ENTER ID HERE: ");
            int id;
            id = int.TryParse(Console.ReadLine(), out id) ? id : -1;
            if (id == -1)
            {
                Console.WriteLine("Not a valid Id");
            }
            else
            {
                Console.Write("ENTER NEW EMPLOYER NAME HERE: ");
                string newEmployeeName = Console.ReadLine();

                employeeLogic.ChangeEmployeeName(id, newEmployeeName);

                var q = employeeLogic.GetEmployeeById(id);

                Console.WriteLine("\n:: NEW EMPLOYEE NAME ::\n");
                Console.WriteLine(q.Name);
            }

            Console.WriteLine("\nPress any key to return");
            Console.ReadLine();
        }

        private static void ChangeJobName(PersonLogic jobLogic)
        {
            Console.Write("ENTER ID HERE: ");
            int id;
            id = int.TryParse(Console.ReadLine(), out id) ? id : -1;
            if (id == -1)
            {
                Console.WriteLine("Not a valid Id");
            }
            else
            {
                Console.Write("ENTER NEW JOB NAME HERE: ");
                string newjobname = Console.ReadLine();

                jobLogic.ChangeJobName(id, newjobname);

                var q = jobLogic.GetJobById(id);

                Console.WriteLine("\n:: NEW JOB NAME ::\n");
                Console.WriteLine(q.JobName);
            }

            Console.WriteLine("\nPress any key to return");
            Console.ReadLine();
        }

        private static void RemoveJob(PersonLogic personLogic)
        {
            Console.WriteLine("ENTER THE ID OF THE JOB YOU WANT TO REMOVE:");
            int id;
            id = int.TryParse(Console.ReadLine(), out id) ? id : -1;
            if (id == -1)
            {
                Console.WriteLine("Not a valid Id");
            }
            else
            {
                personLogic.RemoveJob(id);
            }

            Console.WriteLine("\nPress any key to return");
            Console.ReadLine();
        }

        private static void RemoveSpaceship(SpaceshipLogic spaceshipLogic)
        {
            Console.WriteLine("ENTER THE ID OF THE SPACESHIP YOU WANT TO REMOVE:");
            int id;
            id = int.TryParse(Console.ReadLine(), out id) ? id : -1;
            if (id == -1)
            {
                Console.WriteLine("Not a valid Id");
            }
            else
            {
                spaceshipLogic.Remove(id);
            }

            Console.WriteLine("\nPress any key to return");
            Console.ReadLine();
        }

        private static void RemoveEmployer(PersonLogic personLogic)
        {
            Console.WriteLine("ENTER THE ID OF THE EMPLOYER YOU WANT TO REMOVE:");
            int id;
            id = int.TryParse(Console.ReadLine(), out id) ? id : -1;
            if (id == -1)
            {
                Console.WriteLine("Not a valid Id");
            }
            else
            {
                personLogic.RemoveEmployer(id);
            }

            Console.WriteLine("\nPress any key to return");
            Console.ReadLine();
        }

        private static void RemoveEmployee(PersonLogic personLogic)
        {
            Console.WriteLine("ENTER THE ID OF THE EMPLOYER YOU WANT TO REMOVE:");
            int id;
            id = int.TryParse(Console.ReadLine(), out id) ? id : -1;
            if (id == -1)
            {
                Console.WriteLine("Not a valid Id");
            }
            else
            {
                personLogic.RemoveEmployee(id);
            }

            Console.WriteLine("\nPress any key to return");
            Console.ReadLine();
        }

        private static void AddJob(PersonLogic personLogic)
        {
            Console.WriteLine("ENTER THE ID OF THE JOB YOU WANT TO ADD: ");
            int id;
            id = int.TryParse(Console.ReadLine(), out id) ? id : -1;

            Console.WriteLine("ENTER THE NAME OF THE JOB YOU WANT TO ADD: ");
            string name = Console.ReadLine();

            Console.WriteLine("ENTER THE MINIMUM WAGE OF THE JOB YOU WANT TO ADD: ");
            int min;
            min = int.TryParse(Console.ReadLine(), out min) ? min : -1;

            Console.WriteLine("ENTER THE MAXIMUM WAGE OF THE JOB YOU WANT TO ADD: ");
            int max;
            max = int.TryParse(Console.ReadLine(), out max) ? max : -1;

            Console.WriteLine("ENTER THE DESCRIPTION OF THE JOB YOU WANT TO ADD: ");
            string desc = Console.ReadLine();

            Console.WriteLine("ENTER THE RATING OF THE JOB YOU WANT TO ADD: ");
            int rating;
            rating = int.TryParse(Console.ReadLine(), out rating) ? rating : -1;

            if (id == -1 || min == -1 || max == -1 || rating == 1)
            {
                Console.WriteLine("Not a valid input");
            }
            else
            {
                Job job = new () { JobId = id, JobName = name, MinWage = min, MaxWage = max, JobDescription = desc, JobRating = rating };
                personLogic.InsertJob(job);

                Console.WriteLine($"The job you added: {job.MainData}");
            }

            Console.WriteLine("\nPress any key to return");
            Console.ReadLine();
        }

        private static void AddSpaceship(SpaceshipLogic spaceshipLogic)
        {
            Console.WriteLine("ENTER THE ID OF THE SPACESHIP YOU WANT TO ADD: ");
            int id;
            id = int.TryParse(Console.ReadLine(), out id) ? id : -1;

            Console.WriteLine("ENTER THE NAME OF THE SPACESHIP YOU WANT TO ADD: ");
            string name = Console.ReadLine();

            Console.WriteLine("ENTER THE NUMBER OF THE CREW OF THE SPACESHIP YOU WANT ADD:");
            int crew;
            crew = int.TryParse(Console.ReadLine(), out crew) ? crew : -1;

            Console.WriteLine("ENTER THE ARMAMENT OF THE SPACESHIP YOU WANT ADD:");
            string arm = Console.ReadLine();

            Console.WriteLine("ENTER THE MAXIMUM SPEED OF THE SPACESHIP YOU WANT ADD:");
            int max;
            max = int.TryParse(Console.ReadLine(), out max) ? max : -1;

            Console.WriteLine("ENTER THE SIZE OF THE SPACESHIP YOU WANT ADD:");
            int size;
            size = int.TryParse(Console.ReadLine(), out size) ? size : -1;

            if (id == -1 || crew == -1 || max == -1 || size == -1)
            {
                Console.WriteLine("Not a valid input");
            }
            else
            {
                Spaceship ship = new () { SpaceshipId = id, SpaceshipName = name, Crew = crew, Armament = arm, MaxSpeed = max, Size = size };
                spaceshipLogic.Insert(ship);

                Console.WriteLine($"The spaceship you added: {ship.MainData}");
            }

            Console.ReadLine();
        }

        private static void AddEmployer(PersonLogic personLogic)
        {
            Console.WriteLine("ENTER THE ID OF THE EMPLOYER YOU WANT ADD:");
            int id;
            id = int.TryParse(Console.ReadLine(), out id) ? id : -1;

            Console.WriteLine("ENTER THE NAME OF THE EMPLOYER YOU WANT ADD:");
            string name = Console.ReadLine();

            Console.WriteLine("ENTER THE HOMEWORLD OF THE EMPLOYER YOU WANT ADD:");
            string home = Console.ReadLine();

            Console.WriteLine("ENTER THE AMOUNT OF CREDITS HAS OF THE EMPLOYER YOU WANT ADD:");
            int credit;
            credit = int.TryParse(Console.ReadLine(), out credit) ? credit : -1;

            Console.WriteLine("ENTER THE BOUNTY OF THE EMPLOYER YOU WANT ADD:");
            int bounty;
            bounty = int.TryParse(Console.ReadLine(), out bounty) ? bounty : -1;

            Console.WriteLine("ENTER THE HAIR COLOR OF THE EMPLOYER YOU WANT ADD:");
            string hair = Console.ReadLine();

            Console.WriteLine("ENTER THE ID OF THE SPACESHIP OF THE EMPLOYER YOU WANT ADD:");
            int spaceshipId;
            spaceshipId = int.TryParse(Console.ReadLine(), out spaceshipId) ? spaceshipId : -1;

            Console.WriteLine("ENTER THE ID OF THE JOB OF THE EMPLOYER YOU WANT ADD:");
            int jobId;
            jobId = int.TryParse(Console.ReadLine(), out jobId) ? jobId : -1;

            if (id == -1 || credit == -1 || bounty == -1 || spaceshipId == -1 || jobId == -1)
            {
                Console.WriteLine("Not a valid input");
            }
            else
            {
                Employer emp = new () { Identichip = id, Name = name, Homeworld = home, CreditsOwn = credit, Bounty = bounty, HairColor = hair, SpaceshipId = spaceshipId, JobId = jobId, };
                personLogic.InsertEmployer(emp);

                Console.WriteLine($"The you employer added: {emp.MainData}");
            }

            Console.WriteLine("\nPress any key to return");
            Console.ReadLine();
        }

        private static void AddEmployee(PersonLogic personLogic)
        {
            Console.WriteLine("ENTER THE ID OF THE EMPLOYEE YOU WANT ADD:");
            int id;
            id = int.TryParse(Console.ReadLine(), out id) ? id : -1;

            Console.WriteLine("ENTER THE NAME OF THE EMPLOYEE YOU WANT ADD:");
            string name = Console.ReadLine();

            Console.WriteLine("ENTER THE HOMEWORLD OF THE EMPLOYEE YOU WANT ADD:");
            string home = Console.ReadLine();

            Console.WriteLine("ENTER THE SKILLS OF THE EMPLOYEE YOU WANT ADD:");
            int skills;
            skills = int.TryParse(Console.ReadLine(), out skills) ? skills : -1;

            Console.WriteLine("ENTER THE AMOUNT OF CREDITS WANTED BY THE EMPLOYEE YOU WANT ADD:");
            int credit;
            credit = int.TryParse(Console.ReadLine(), out credit) ? credit : -1;

            Console.WriteLine("ENTER THE BOUNTY OF THE EMPLOYEE YOU WANT ADD:");
            int bounty;
            bounty = int.TryParse(Console.ReadLine(), out bounty) ? bounty : -1;

            Console.WriteLine("ENTER THE ID OF THE SPACESHIP OF THE EMPLOYEE YOU WANT ADD:");
            int spaceshipId;
            spaceshipId = int.TryParse(Console.ReadLine(), out spaceshipId) ? spaceshipId : -1;

            Console.WriteLine("ENTER THE ID OF THE JOB OF THE EMPLOYEE YOU WANT ADD:");
            int jobId;
            jobId = int.TryParse(Console.ReadLine(), out jobId) ? jobId : -1;

            Console.WriteLine("ENTER THE ID OF THE EMPLOYER OF THE EMPLOYEE YOU WANT ADD:");
            int employerId;
            employerId = int.TryParse(Console.ReadLine(), out employerId) ? employerId : -1;

            if (id == -1 || skills == -1 || credit == -1 || bounty == -1 || spaceshipId == -1 || jobId == -1 || employerId == -1)
            {
                Console.WriteLine("Not a valid input");
            }
            else
            {
                Employee emp = new () { Identichip = id, Name = name, Homeworld = home, Skills = skills, CreditsWanted = credit, Bounty = bounty, SpaceshipId = spaceshipId, JobId = jobId, EmployerId = employerId };
                personLogic.AddEmployee(emp);

                Console.WriteLine($"The you employee added: {emp.MainData}");
            }

            Console.WriteLine("\nPress any key to return");
            Console.ReadLine();
        }
    }
}