﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("", "CA1014", Justification = "<NikGitStats>", Scope = "module")]
[assembly: SuppressMessage("Globalization", "CA1307:Specify StringComparison for clarity", Justification = "<NikGitStats>", Scope = "member", Target = "~M:MosEisley.Logic.SameEmployeeAndEmployerHomeworldResult.GetHashCode~System.Int32")]
[assembly: SuppressMessage("Globalization", "CA1307:Specify StringComparison for clarity", Justification = "<NikGitStats>", Scope = "member", Target = "~M:MosEisley.Logic.AvgSkillResult.GetHashCode~System.Int32")]
[assembly: SuppressMessage("Globalization", "CA1307:Specify StringComparison for clarity", Justification = "<NikGitStats>", Scope = "member", Target = "~M:MosEisley.Logic.PersonLogic.SameEmployeeAndEmployerHomeworld~System.Collections.Generic.IList{MosEisley.Logic.SameEmployeeAndEmployerHomeworldResult}")]
[assembly: SuppressMessage("Globalization", "CA1307:Specify StringComparison for clarity", Justification = "<NikGitStats>", Scope = "member", Target = "~M:MosEisley.Logic.Top3FastestShipOwnerResult.GetHashCode~System.Int32")]
[assembly: SuppressMessage("Globalization", "CA1309:Use ordinal string comparison", Justification = "<NikGitStats>", Scope = "member", Target = "~M:MosEisley.Logic.PersonLogic.SameEmployeeAndEmployerHomeworld~System.Collections.Generic.IList{MosEisley.Logic.SameEmployeeAndEmployerHomeworldResult}")]
[assembly: SuppressMessage("Globalization", "CA1307:Specify StringComparison for clarity", Justification = "<NikGitStats>", Scope = "member", Target = "~M:MosEisley.Logic.SumCreditsResult.GetHashCode~System.Int32")]
[assembly: SuppressMessage("Globalization", "CA1307:Specify StringComparison for clarity", Justification = "<NikGitStats>", Scope = "member", Target = "~M:MosEisley.Logic.AvgSpeedResult.GetHashCode~System.Int32")]
