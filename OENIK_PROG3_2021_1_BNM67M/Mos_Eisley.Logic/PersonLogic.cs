﻿// <copyright file="PersonLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MosEisley.Logic
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using MosEisley.Data.Beta.Models;
    using MosEisley.Repository;

    /// <summary>
    /// This class contains the logic of Person.
    /// </summary>
    public class PersonLogic : IPersonLogic
    {
        private readonly IEmployeeRepository employeeRepo;
        private readonly IJobRepository jobRepo;
        private readonly IEmployerRepository employerRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="PersonLogic"/> class.
        /// </summary>
        /// <param name="employeeRepo">The repository of Employee.</param>
        /// <param name="jobRepo">The repository of Job.</param>
        /// <param name="employerRepo">The repository of Employer.</param>
        public PersonLogic(IEmployeeRepository employeeRepo, IJobRepository jobRepo, IEmployerRepository employerRepo)
        {
            this.employeeRepo = employeeRepo;
            this.jobRepo = jobRepo;
            this.employerRepo = employerRepo;
        }

        /// <inheritdoc/>
        public void ChangeHomeworldEmployee(int id, string homeworld)
        {
            this.employeeRepo.ChangeHomeworld(id, homeworld);
        }

        /// <inheritdoc/>
        public void ChangeEmployeeName(int id, string newname)
        {
            this.employeeRepo.ChangeName(id, newname);
        }

        /// <inheritdoc/>
        public void ChangeSkillsEmployee(int id, int newskills)
        {
            this.employeeRepo.ChangeSkills(id, newskills);
        }

        /// <inheritdoc/>
        public void RemoveEmployee(int id)
        {
            this.employeeRepo.Remove(id);
        }

        /// <inheritdoc/>
        public IList<Employee> GetAllEmployees()
        {
            return this.employeeRepo.GetAll().ToList();
        }

        /// <inheritdoc/>
        public Employee GetEmployeeById(int id)
        {
            return this.employeeRepo.GetOne(id);
        }

        /// <inheritdoc/>
        public void AddEmployee(Employee employee)
        {
            this.employeeRepo.Add(employee);
        }

        /// <inheritdoc/>
        public void ChangeEmployerHomeworld(int id, string homeworld)
        {
            this.employerRepo.ChangeHomeworld(id, homeworld);
        }

        /// <inheritdoc/>
        public void ChangeEmployerName(int id, string newname)
        {
            this.employerRepo.ChangeName(id, newname);
        }

        /// <inheritdoc/>
        public IList<Employer> GetAllEmployers()
        {
            return this.employerRepo.GetAll().ToList();
        }

        /// <inheritdoc/>
        public Employer GetEmployerById(int id)
        {
            return this.employerRepo.GetOne(id);
        }

        /// <inheritdoc/>
        public void InsertEmployer(Employer employer)
        {
            this.employerRepo.Add(employer);
        }

        /// <inheritdoc/>
        public void RemoveEmployer(int id)
        {
            this.employerRepo.Remove(id);
        }

        /// <inheritdoc/>
        public void ChangeJobName(int id, string newname)
        {
            this.jobRepo.ChangeJobName(id, newname);
        }

        /// <inheritdoc/>
        public IList<Job> GetAllJobs()
        {
            return this.jobRepo.GetAll().ToList();
        }

        /// <inheritdoc/>
        public Job GetJobById(int id)
        {
            return this.jobRepo.GetOne(id);
        }

        /// <inheritdoc/>
        public void InsertJob(Job job)
        {
            this.jobRepo.Add(job);
        }

        /// <inheritdoc/>
        public void RemoveJob(int id)
        {
            this.jobRepo.Remove(id);
        }

        /// <inheritdoc/>
        public IList<SameEmployeeAndEmployerHomeworldResult> SameEmployeeAndEmployerHomeworld()
        {
            var q = this.employeeRepo.GetAll()
                .Where(x => x.Homeworld.Equals(x.Employer.Homeworld))
                .Select(x => new SameEmployeeAndEmployerHomeworldResult
                {
                    EmployeeName = x.Name,
                    EmployerName = x.Employer.Name,
                    SameHomeworld = x.Homeworld,
                });

            return q.ToList();
        }

        /// <inheritdoc/>
        public IList<AvgSkillResult> GetJobAvg()
        {
            var q = from emp in this.employeeRepo.GetAll()
                    join job in this.jobRepo.GetAll() on emp.JobId equals job.JobId
                    let item = new { job.JobName, emp.Skills }
                    group item by item.JobName into grp
                    select new AvgSkillResult()
                    {
                        JobName = grp.Key,
                        AverageSkills = (double)grp.Average(item => item.Skills),
                    };
            return q.ToList();
        }

        /// <inheritdoc/>
        public IList<SumCreditsResult> GetJobSumCredits()
        {
            var q = from emp in this.employerRepo.GetAll()
                    join job in this.jobRepo.GetAll() on emp.JobId equals job.JobId
                    let item = new { job.JobName, emp.CreditsOwn }
                    group item by item.JobName into grp
                    select new SumCreditsResult()
                    {
                        JobName = grp.Key,
                        SumCredits = (int)grp.Sum(item => item.CreditsOwn),
                    };
            return q.ToList();
        }

        /// <summary>
        /// Async version of SameEmployeeAndEmployerHomeworlsAsync.
        /// </summary>
        /// <returns>Task.</returns>
        public Task<IList<SameEmployeeAndEmployerHomeworldResult>> SameEmployeeAndEmployerHomeworldAsync()
        {
            return Task.Run(() => this.SameEmployeeAndEmployerHomeworld());
        }

        /// <summary>
        /// Async version of GetJobAvg.
        /// </summary>
        /// <returns>Task.</returns>
        public Task<IList<AvgSkillResult>> GetJobAvgAsync()
        {
            return Task.Run(() => this.GetJobAvg());
        }

        /// <summary>
        /// Async version of GetJobSumCredits.
        /// </summary>
        /// <returns>Task.</returns>
        public Task<IList<SumCreditsResult>> GetJobSumCreditsAsync()
        {
            return Task.Run(() => this.GetJobSumCredits());
        }
    }
}
