﻿// <copyright file="AvgSpeedResult.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MosEisley.Logic
{
    /// <summary>
    /// Contains AvgSpeedResult properties, set them up and methods.
    /// </summary>
    public class AvgSpeedResult
    {
        /// <summary>
        /// Gets or sets Homeworld name.
        /// </summary>
        public string HomeworldName { get; set; }

        /// <summary>
        /// Gets or sets average speed.
        /// </summary>
        public double AverageSpeed { get; set; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"Planet: {this.HomeworldName} - Average speed of the spachships: {this.AverageSpeed}";
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is AvgSpeedResult)
            {
                AvgSpeedResult other = obj as AvgSpeedResult;
                return this.HomeworldName == other.HomeworldName &&
                   this.AverageSpeed == other.AverageSpeed;
            }
            else
            {
                return false;
            }
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return this.HomeworldName.GetHashCode() + (int)this.AverageSpeed;
        }
    }
}
