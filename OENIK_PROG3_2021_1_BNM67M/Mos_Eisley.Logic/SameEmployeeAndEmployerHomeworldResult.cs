﻿// <copyright file="SameEmployeeAndEmployerHomeworldResult.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MosEisley.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Contains SameEmployeeandEmployerHomeworldResult properties, set them up and methods.
    /// </summary>
    public class SameEmployeeAndEmployerHomeworldResult
    {
        /// <summary>
        /// Gets or sets Employee name.
        /// </summary>
        public string EmployeeName { get; set; }

        /// <summary>
        /// Gets or sets Employer name.
        /// </summary>
        public string EmployerName { get; set; }

        /// <summary>
        /// Gets or sets Same Homeworld name.
        /// </summary>
        public string SameHomeworld { get; set; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"{this.EmployeeName} and {this.EmployerName} are from {this.SameHomeworld}";
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is SameEmployeeAndEmployerHomeworldResult)
            {
                SameEmployeeAndEmployerHomeworldResult other = obj as SameEmployeeAndEmployerHomeworldResult;
                return this.EmployeeName == other.EmployeeName &&
                   this.EmployerName == other.EmployerName &&
                   this.SameHomeworld == other.SameHomeworld;
            }
            else
            {
                return false;
            }
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return this.EmployeeName.GetHashCode() + this.EmployerName.GetHashCode() + this.SameHomeworld.GetHashCode();
        }
    }
}
