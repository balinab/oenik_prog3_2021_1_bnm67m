﻿// <copyright file="IPersonLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MosEisley.Logic
{
    using System.Collections.Generic;
    using MosEisley.Data.Beta.Models;

    /// <summary>
    /// This class contains methods for PersonLogic.
    /// </summary>
    public interface IPersonLogic
    {
        /// <summary>
        /// This method accomplish GetEmployeeById.
        /// </summary>
        /// <param name="id">The id which identify the employee.</param>
        /// <returns>The employee object. </returns>
        Employee GetEmployeeById(int id);

        /// <summary>
        /// This method accomplish ChangeNameEmployee.
        /// </summary>
        /// <param name="id">The id which identify the employee.</param>
        /// <param name="newname">The new name which will be given.</param>
        void ChangeEmployeeName(int id, string newname);

        /// <summary>
        /// This method accomplish ChangeHomeworldEmployee.
        /// </summary>
        /// <param name="id">The id which identify the employee.</param>
        /// <param name="homeworld">The new homeworld which will be given.</param>
        void ChangeHomeworldEmployee(int id, string homeworld);

        /// <summary>
        /// This method accomplish ChangeSkillsEmployee.
        /// </summary>
        /// <param name="id">The id which identify the employee.</param>
        /// <param name="newskills">The new skills which will be given.</param>
        void ChangeSkillsEmployee(int id, int newskills);

        /// <summary>
        /// List all employees.
        /// </summary>
        /// <returns>A list of all the employees.</returns>
        IList<Employee> GetAllEmployees();

        /// <summary>
        /// Create a new employee.
        /// </summary>
        /// <param name="employee">Set up employee an object.</param>
        void AddEmployee(Employee employee);

        /// <summary>
        /// Remove an employee by id.
        /// </summary>
        /// <param name="id">The id which identify the employee.</param>
        void RemoveEmployee(int id);

        /// <summary>
        /// This interface accomplish GetEmployerById.
        /// </summary>
        /// <param name="id">The id which identify the employer.</param>
        /// <returns>The employer object. </returns>
        Employer GetEmployerById(int id);

        /// <summary>
        /// This method accomplish ChangeNameEmployer.
        /// </summary>
        /// <param name="id">The id which identify the employee.</param>
        /// <param name="newname">The new name which will be given.</param>
        void ChangeEmployerName(int id, string newname);

        /// <summary>
        /// This method accomplish ChangeHomeworldEmployer.
        /// </summary>
        /// <param name="id">The id which identify the employee.</param>
        /// <param name="homeworld">The new homeworld which will be given.</param>
        void ChangeEmployerHomeworld(int id, string homeworld);

        /// <summary>
        /// List all employers.
        /// </summary>
        /// <returns>A list of all the employers.</returns>
        IList<Employer> GetAllEmployers();

        /// <summary>
        /// Create a new employer.
        /// </summary>
        /// <param name="employer">Set up employer an object.</param>
        void InsertEmployer(Employer employer);

        /// <summary>
        /// Remove an employer by id.
        /// </summary>
        /// <param name="id">The id which identify the employer.</param>
        void RemoveEmployer(int id);

        /// <summary>
        /// This interface accomplish GetJobById.
        /// </summary>
        /// <param name="id">The id which identify the job.</param>
        /// <returns>The employer object.</returns>
        Job GetJobById(int id);

        /// <summary>
        /// This method accomplish ChangeJobName.
        /// </summary>
        /// <param name="id">The id which identify the job.</param>
        /// <param name="newname">The new name which will be given.</param>
        void ChangeJobName(int id, string newname);

        /// <summary>
        /// List all jobs.
        /// </summary>
        /// <returns>A list of all the jobs.</returns>
        IList<Job> GetAllJobs();

        /// <summary>
        /// Create a new job.
        /// </summary>
        /// <param name="job">Set up job an object.</param>
        void InsertJob(Job job);

        /// <summary>
        /// Remove a job by id.
        /// </summary>
        /// <param name="id">The id which identify the job.</param>
        void RemoveJob(int id);

        /// <summary>
        /// Get the same homeworld employer and employees.
        /// </summary>
        /// <returns>A list of people who has the same homeworld.</returns>
        IList<SameEmployeeAndEmployerHomeworldResult> SameEmployeeAndEmployerHomeworld();

        /// <summary>
        /// Get the average of the skills by jobs.
        /// </summary>
        /// <returns>A list of job names and the average skills of them.</returns>
        IList<AvgSkillResult> GetJobAvg();

        /// <summary>
        /// Get the all the amount of credits own by employers who wants person for the same job.
        /// </summary>
        /// <returns>A list of job names and the summerized credits of them.</returns>
        IList<SumCreditsResult> GetJobSumCredits();
    }
}
