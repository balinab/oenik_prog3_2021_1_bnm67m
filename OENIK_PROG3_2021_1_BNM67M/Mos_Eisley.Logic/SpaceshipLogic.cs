﻿// <copyright file="SpaceshipLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MosEisley.Logic
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using MosEisley.Data.Beta.Models;
    using MosEisley.Repository;

    /// <summary>
    /// This class accomplish ISpaceship methods.
    /// </summary>
    public class SpaceshipLogic : ISpaceshipLogic
    {
        private readonly ISpaceshipRepository spaceshipRepo;
        private readonly IEmployeeRepository employeeRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="SpaceshipLogic"/> class.
        /// </summary>
        /// <param name="repo">Spaceship repository.</param>
        /// <param name="empRepo">Employee repository.</param>
        public SpaceshipLogic(ISpaceshipRepository repo, IEmployeeRepository empRepo)
        {
            this.spaceshipRepo = repo;
            this.employeeRepo = empRepo;
        }

        /// <inheritdoc/>
        public void ChangeSpaceshipName(int id, string newname)
        {
            this.spaceshipRepo.ChangeSpaceshipName(id, newname);
        }

        /// <inheritdoc/>
        public void ChangeCrew(int id, int newcrew)
        {
            this.spaceshipRepo.ChangeCrew(id, newcrew);
        }

        /// <inheritdoc/>
        public void ChangeArmament(int id, string newarmament)
        {
            this.spaceshipRepo.ChangeArmament(id, newarmament);
        }

        /// <inheritdoc/>
        public void ChangeMaximumSpeed(int id, int newspeed)
        {
            this.spaceshipRepo.ChangeMaximumSpeed(id, newspeed);
        }

        /// <inheritdoc/>
        public IList<Spaceship> GetAllSpaceships()
        {
            return this.spaceshipRepo.GetAll().ToList();
        }

        /// <inheritdoc/>
        public Spaceship GetSpaceshipById(int id)
        {
            return this.spaceshipRepo.GetOne(id);
        }

        /// <inheritdoc/>
        public void Insert(Spaceship spaceship)
        {
            this.spaceshipRepo.Add(spaceship);
        }

        /// <inheritdoc/>
        public void Remove(int id)
        {
            this.spaceshipRepo.Remove(id);
        }

        /// <inheritdoc/>
        public IList<Top3FastestShipOwnerResult> GetTop3FastestShipOwnerEmployees()
        {
            var q = this.employeeRepo.GetAll()
                .Where(x => x.SpaceshipId.Equals(x.Spaceship.SpaceshipId))
                .OrderByDescending(x => x.Spaceship.MaxSpeed)
                .Select(x => new Top3FastestShipOwnerResult
                {
                    OwnerName = x.Name,
                    ShipName = x.Spaceship.SpaceshipName,
                    MaximumSpeed = (int)x.Spaceship.MaxSpeed,
                });
            return q.Take(3).ToList();
        }

        /// <summary>
        /// Async version of GetTop3FAstestShipOwners.
        /// </summary>
        /// <returns>Task.</returns>
        public Task<IList<Top3FastestShipOwnerResult>> GetTop3FastestShipOwnerEmployeesAsync()
        {
            return Task.Run(() => this.GetTop3FastestShipOwnerEmployees());
        }

        /// <inheritdoc/>
        public IList<AvgSpeedResult> GetSpeedAvg()
        {
            var q = from ship in this.spaceshipRepo.GetAll()
                    join emp in this.employeeRepo.GetAll() on ship.SpaceshipId equals emp.SpaceshipId
                    let item = new { emp.Homeworld, ship.MaxSpeed }
                    group item by item.Homeworld into grp
                    select new AvgSpeedResult()
                    {
                        HomeworldName = grp.Key,
                        AverageSpeed = (double)grp.Average(item => item.MaxSpeed),
                    };
            return q.ToList();
        }

        /// <summary>
        /// Async version of GetSpeedAvg.
        /// </summary>
        /// <returns>Task.</returns>
        public Task<IList<AvgSpeedResult>> GetSpeedAvgAsync()
        {
            return Task.Run(() => this.GetSpeedAvg());
        }
    }
}
