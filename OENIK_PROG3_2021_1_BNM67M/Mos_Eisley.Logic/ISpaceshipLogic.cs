﻿// <copyright file="ISpaceshipLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MosEisley.Logic
{
    using System.Collections.Generic;
    using MosEisley.Data.Beta.Models;

    /// <summary>
    /// This class contains methods for SpaceshipLogic.
    /// </summary>
    public interface ISpaceshipLogic
    {
        /// <summary>
        /// This method accomplish SpaceshipById.
        /// </summary>
        /// <param name="id">The id which identify the spaceship.</param>
        /// <returns>The Spaceship object.</returns>
        Spaceship GetSpaceshipById(int id);

        /// <summary>
        /// This method accomplish ChangeSpaceshipName.
        /// </summary>
        /// <param name="id">The id which identify the spaceship.</param>
        /// <param name="newname">The new name which will be given.</param>
        void ChangeSpaceshipName(int id, string newname);

        /// <summary>
        /// This method accomplish ChangeCrew.
        /// </summary>
        /// <param name="id">The id which identify the spaceship.</param>
        /// <param name="newcrew">The new crew number which will be given.</param>
        void ChangeCrew(int id, int newcrew);

        /// <summary>
        /// This method accomplish ChangeArmament.
        /// </summary>
        /// <param name="id">The id which identify the spaceship.</param>
        /// <param name="newarmament">The new armament which will be given.</param>
        void ChangeArmament(int id, string newarmament);

        /// <summary>
        /// This method accomplish ChangeMaximumSpeed.
        /// </summary>
        /// <param name="id">The id which identify the spaceship.</param>
        /// <param name="newspeed">The new max speed which will be given.</param>
        void ChangeMaximumSpeed(int id, int newspeed);

        /// <summary>
        /// List all spaceships.
        /// </summary>
        /// <returns>A list of all the spaceships.</returns>
        IList<Spaceship> GetAllSpaceships();

        /// <summary>
        /// Creat a new spaceship.
        /// </summary>
        /// <param name="spaceship">Set up spaceship an object.</param>
        void Insert(Spaceship spaceship);

        /// <summary>
        /// Remove an spaceship by id.
        /// </summary>
        /// <param name="id">The id which identify the spaceship.</param>
        void Remove(int id);

        /// <summary>
        /// Get the top3 maximum speed employee's ship.
        /// </summary>
        /// <returns>A list of the fastest ships an owners.</returns>
        IList<Top3FastestShipOwnerResult> GetTop3FastestShipOwnerEmployees();

        /// <summary>
        /// Get the average of the speed of spaceships amongst the employees by homeworld.
        /// </summary>
        /// <returns>A list of planets names and the average speed of them.</returns>
        IList<AvgSpeedResult> GetSpeedAvg();
    }
}
