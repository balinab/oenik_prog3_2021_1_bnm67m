﻿// <copyright file="SumCreditsResult.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MosEisley.Logic
{
    /// <summary>
    /// Contains SumCreditslResult properties, set them up and methods.
    /// </summary>
    public class SumCreditsResult
    {
        /// <summary>
        /// Gets or sets Job name.
        /// </summary>
        public string JobName { get; set; }

        /// <summary>
        /// Gets or sets of the summerized credits.
        /// </summary>
        public int SumCredits { get; set; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"Job: {this.JobName} - All credits owned: {this.SumCredits}";
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is SumCreditsResult)
            {
                SumCreditsResult other = obj as SumCreditsResult;
                return this.JobName == other.JobName &&
                   this.SumCredits == other.SumCredits;
            }
            else
            {
                return false;
            }
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return this.JobName.GetHashCode() + (int)this.SumCredits;
        }
    }
}
