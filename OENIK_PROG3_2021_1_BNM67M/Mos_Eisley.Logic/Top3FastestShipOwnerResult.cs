﻿// <copyright file="Top3FastestShipOwnerResult.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MosEisley.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Contains Top3FastestShipOwnerResult properties, set them up and methods.
    /// </summary>
    public class Top3FastestShipOwnerResult
    {
        /// <summary>
        /// Gets or sets of the spaceship owner name.
        /// </summary>
        public string OwnerName { get; set; }

        /// <summary>
        /// Gets or sets of the the ship name.
        /// </summary>
        public string ShipName { get; set; }

        /// <summary>
        /// Gets or sets maximum amount of speed of a ship.
        /// </summary>
        public int MaximumSpeed { get; set; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"{this.OwnerName}'s {this.ShipName} maximum speed is {this.MaximumSpeed}.";
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is Top3FastestShipOwnerResult)
            {
                Top3FastestShipOwnerResult other = obj as Top3FastestShipOwnerResult;
                return this.OwnerName == other.OwnerName &&
                    this.ShipName == other.ShipName &&
                   this.MaximumSpeed == other.MaximumSpeed;
            }
            else
            {
                return false;
            }
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return this.OwnerName.GetHashCode() + this.ShipName.GetHashCode() + this.MaximumSpeed;
        }
    }
}
