﻿// <copyright file="AvgSkillResult.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MosEisley.Logic
{
    /// <summary>
    /// Contains AvgSkillResult properties, set them up and methods.
    /// </summary>
    public class AvgSkillResult
    {
        /// <summary>
        /// Gets or sets Job name.
        /// </summary>
        public string JobName { get; set; }

        /// <summary>
        /// Gets or sets average skills.
        /// </summary>
        public double AverageSkills { get; set; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"Job: {this.JobName} - Average skills: {this.AverageSkills}";
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is AvgSkillResult)
            {
                AvgSkillResult other = obj as AvgSkillResult;
                return this.JobName == other.JobName &&
                   this.AverageSkills == other.AverageSkills;
            }
            else
            {
                return false;
            }
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return this.JobName.GetHashCode() + (int)this.AverageSkills;
        }
    }
}
