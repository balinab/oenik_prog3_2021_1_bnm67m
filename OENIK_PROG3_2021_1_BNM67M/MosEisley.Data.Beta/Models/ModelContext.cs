﻿// <copyright file="ModelContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MosEisley.Data.Beta.Models
{
    using System;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// ModelContext class.
    /// </summary>
    public partial class ModelContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ModelContext"/> class.
        /// </summary>
        public ModelContext()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ModelContext"/> class.
        /// </summary>
        /// <param name="options">options.</param>
        public ModelContext(DbContextOptions<ModelContext> options)
            : base(options)
        {
        }

        /// <summary>
        /// Gets or sets dbSet of Employees.
        /// </summary>
        public virtual DbSet<Employee> Employees { get; set; }

        /// <summary>
        /// Gets or sets dbSet of Employers.
        /// </summary>
        public virtual DbSet<Employer> Employers { get; set; }

        /// <summary>
        /// Gets or sets dbSet of Jobs.
        /// </summary>
        public virtual DbSet<Job> Jobs { get; set; }

        /// <summary>
        /// Gets or sets dbSet of Spacehips.
        /// </summary>
        public virtual DbSet<Spaceship> Spaceships { get; set; }

        /// <inheritdoc/>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder is null)
            {
                string message = "optionBuilder is null";
                throw new ArgumentNullException(message);
            }

            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\MosEisleyDB.mdf;Integrated Security=True");
            }
        }

        /// <inheritdoc/>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            if (modelBuilder is null)
            {
                string message = "modelBuilder is null";
                throw new ArgumentNullException(message);
            }

            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.HasKey(e => e.Identichip)
                    .HasName("Employee_pk");

                entity.ToTable("Employee");

                entity.Property(e => e.Identichip).HasColumnType("numeric(20, 0)");

                entity.Property(e => e.Bounty).HasColumnType("numeric(20, 0)");

                entity.Property(e => e.CreditsWanted).HasColumnType("numeric(20, 0)");

                entity.Property(e => e.EmployerId).HasColumnType("numeric(20, 0)");

                entity.Property(e => e.Homeworld)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.JobId).HasColumnType("numeric(20, 0)");

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Skills).HasColumnType("numeric(2, 0)");

                entity.Property(e => e.SpaceshipId).HasColumnType("numeric(20, 0)");

                entity.HasOne(d => d.Employer)
                    .WithMany(p => p.Employees)
                    .HasForeignKey(d => d.EmployerId)
                    .HasConstraintName("employee_fk_employer");

                entity.HasOne(d => d.Job)
                    .WithMany(p => p.Employees)
                    .HasForeignKey(d => d.JobId)
                    .HasConstraintName("Employee_fk_job");

                entity.HasOne(d => d.Spaceship)
                    .WithMany(p => p.Employees)
                    .HasForeignKey(d => d.SpaceshipId)
                    .HasConstraintName("Employee_fk_spaceship");
            });

            modelBuilder.Entity<Employer>(entity =>
            {
                entity.HasKey(e => e.Identichip)
                    .HasName("Employer_pk");

                entity.ToTable("Employer");

                entity.Property(e => e.Identichip).HasColumnType("numeric(20, 0)");

                entity.Property(e => e.Bounty).HasColumnType("numeric(20, 0)");

                entity.Property(e => e.CreditsOwn).HasColumnType("numeric(20, 0)");

                entity.Property(e => e.HairColor)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Homeworld)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.JobId).HasColumnType("numeric(20, 0)");

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SpaceshipId).HasColumnType("numeric(20, 0)");

                entity.HasOne(d => d.Job)
                    .WithMany(p => p.Employers)
                    .HasForeignKey(d => d.JobId)
                    .HasConstraintName("employer_fk_job");

                entity.HasOne(d => d.Spaceship)
                    .WithMany(p => p.Employers)
                    .HasForeignKey(d => d.SpaceshipId)
                    .HasConstraintName("Employer_fk_spaceship");
            });

            modelBuilder.Entity<Job>(entity =>
            {
                entity.ToTable("Job");

                entity.Property(e => e.JobId).HasColumnType("numeric(20, 0)");

                entity.Property(e => e.JobDescription)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.JobName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.JobRating).HasColumnType("numeric(20, 0)");

                entity.Property(e => e.MaxWage).HasColumnType("numeric(20, 0)");

                entity.Property(e => e.MinWage).HasColumnType("numeric(20, 0)");
            });

            modelBuilder.Entity<Spaceship>(entity =>
            {
                entity.ToTable("Spaceship");

                entity.Property(e => e.SpaceshipId).HasColumnType("numeric(20, 0)");

                entity.Property(e => e.Armament)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Crew).HasColumnType("numeric(20, 0)");

                entity.Property(e => e.MaxSpeed).HasColumnType("numeric(10, 0)");

                entity.Property(e => e.Size).HasColumnType("numeric(10, 0)");

                entity.Property(e => e.SpaceshipName)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            this.OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
