﻿// <copyright file="Employee.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MosEisley.Data.Beta.Models
{
    /// <summary>
    /// Employee class containting properties, foreign keys and other methods relevant to it.
    /// </summary>
    public class Employee
    {
        /// <summary>
        /// Gets or sets of Identichip which is the primary key of Employee.
        /// </summary>
        public int Identichip { get; set; }

        /// <summary>
        /// Gets or sets of Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets of Homeworld which is the home of the Employee.
        /// </summary>
        public string Homeworld { get; set; }

        /// <summary>
        /// Gets or sets of Skills which is  rating between 1 to 10.
        /// </summary>
        public int? Skills { get; set; }

        /// <summary>
        /// Gets or sets the amount of credits wanted by an employee.
        /// </summary>
        public decimal? CreditsWanted { get; set; }

        /// <summary>
        /// Gets or sets the bounty on an employees head.
        /// </summary>
        public decimal? Bounty { get; set; }

        /// <summary>
        /// Gets or sets of SpaceshipsId which is a foreign key of the Spaceship SpaceshipId.
        /// </summary>
        public int? SpaceshipId { get; set; }

        /// <summary>
        /// Gets or sets of JobId which is a foreign key of the Job JobId.
        /// </summary>
        public int? JobId { get; set; }

        /// <summary>
        /// Gets or sets of EmployerId which is a foreign key of the Employer Identichip.
        /// </summary>
        public int? EmployerId { get; set; }

        /// <summary>
        /// Gets or sets relation with Employer.
        /// </summary>
        public virtual Employer Employer { get; set; }

        /// <summary>
        /// Gets or sets relation with Job.
        /// </summary>
        public virtual Job Job { get; set; }

        /// <summary>
        /// Gets or sets relation with Spaceship.
        /// </summary>
        public virtual Spaceship Spaceship { get; set; }

        /// <summary>
        /// Gets of MainData which helps to write employee data.
        /// </summary>
        public string MainData => $"[{this.Identichip}] : {this.Name} : {this.Homeworld} : (Skills: {this.Skills}) : (Wants: {this.CreditsWanted} credit) : (Bounty on its head: {this.Bounty} credit)";
    }
}
