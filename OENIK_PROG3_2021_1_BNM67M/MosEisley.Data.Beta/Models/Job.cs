﻿// <copyright file="Job.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MosEisley.Data.Beta.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// Job class containting properties, foreign keys and other methods relevant to it.
    /// </summary>
    public class Job
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Job"/> class.
        /// </summary>
        public Job()
        {
            this.Employees = new HashSet<Employee>();
            this.Employers = new HashSet<Employer>();
        }

        /// <summary>
        /// Gets or sets of JobId which is the primary key of Job.
        /// </summary>
        public int JobId { get; set; }

        /// <summary>
        /// Gets or sets of JobName.
        /// </summary>
        public string JobName { get; set; }

        /// <summary>
        /// Gets or sets the minimum wage of a job.
        /// </summary>
        public decimal? MinWage { get; set; }

        /// <summary>
        /// Gets or sets the maximum wage of a job.
        /// </summary>
        public decimal? MaxWage { get; set; }

        /// <summary>
        /// Gets or sets description of a job.
        /// </summary>
        public string JobDescription { get; set; }

        /// <summary>
        /// Gets or sets the rating of a job.
        /// </summary>
        public decimal? JobRating { get; set; }

        /// <summary>
        /// Gets iCollention of Employees.
        /// </summary>
        public virtual ICollection<Employee> Employees { get; }

        /// <summary>
        /// Gets iCollention of Employers.
        /// </summary>
        public virtual ICollection<Employer> Employers { get; }

        /// <summary>
        /// Gets of MainData which helps to write Job data.
        /// </summary>
        public string MainData => $"[{this.JobId}] : {this.JobName} : (Minimum wage: {this.MinWage} credit) : (Maximum wage: {this.MaxWage} credit) : (Description: {this.JobDescription}) : (Rating: {this.JobRating})";
    }
}
