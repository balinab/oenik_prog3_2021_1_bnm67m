﻿// <copyright file="Spaceship.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MosEisley.Data.Beta.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// Spaceship class containting properties, foreign keys and other methods relevant to it.
    /// </summary>
    public partial class Spaceship
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Spaceship"/> class.
        /// </summary>
        public Spaceship()
        {
            this.Employees = new HashSet<Employee>();
            this.Employers = new HashSet<Employer>();
        }

        /// <summary>
        /// Gets or sets of SpaceshipId which is the primary key of Spaceship.
        /// </summary>
        public int SpaceshipId { get; set; }

        /// <summary>
        /// Gets or sets of SpaceshipName.
        /// </summary>
        public string SpaceshipName { get; set; }

        /// <summary>
        /// Gets or sets number of the crew.
        /// </summary>
        public int? Crew { get; set; }

        /// <summary>
        /// Gets or sets of Armament of the ship.
        /// </summary>
        public string Armament { get; set; }

        /// <summary>
        /// Gets or sets of MaxSpeed which is the number of the maximum speed of the ship.
        /// </summary>
        public int? MaxSpeed { get; set; }

        /// <summary>
        /// Gets or sets of the size of a  spaceship.
        /// </summary>
        public decimal? Size { get; set; }

        /// <summary>
        /// Gets iColletion of Employees.
        /// </summary>
        public virtual ICollection<Employee> Employees { get; }

        /// <summary>
        /// Gets IColletion of Employers.
        /// </summary>
        public virtual ICollection<Employer> Employers { get; }

        /// <summary>
        /// Gets MainData.
        /// </summary>
        public string MainData => $"[{this.SpaceshipId}] : {this.SpaceshipName} : (Crew: {this.Crew}) : (Armament: {this.Armament}) : (MaxSpeed: {this.MaxSpeed}) : (Size: {this.Size})";
    }
}