﻿// <copyright file="Employer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MosEisley.Data.Beta.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// Employer class containting properties, foreign keys and other methods relevant to it.
    /// </summary>
    public class Employer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Employer"/> class.
        /// </summary>
        public Employer()
        {
            this.Employees = new HashSet<Employee>();
        }

        /// <summary>
        /// Gets or sets of Identichip which is the primary key of Employer.
        /// </summary>
        public int Identichip { get; set; }

        /// <summary>
        /// Gets or sets of Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets of Homeworld which is the home of the Employer.
        /// </summary>
        public string Homeworld { get; set; }

        /// <summary>
        /// Gets or sets the amount of credits owned by an employer.
        /// </summary>
        public decimal? CreditsOwn { get; set; }

        /// <summary>
        /// Gets or sets the bounty on an employers head.
        /// </summary>
        public decimal? Bounty { get; set; }

        /// <summary>
        /// Gets or sets the color of the hair of the employer.
        /// </summary>
        public string HairColor { get; set; }

        /// <summary>
        /// Gets or sets of SpaceshipsId which is a foreign key of the Spaceship SpaceshipId.
        /// </summary>
        public int? SpaceshipId { get; set; }

        /// <summary>
        /// Gets or sets of JobId which is a foreign key of the Job JobId.
        /// </summary>
        public int? JobId { get; set; }

        /// <summary>
        /// Gets or sets relation with Job.
        /// </summary>
        public virtual Job Job { get; set; }

        /// <summary>
        /// Gets or sets relation with Spaceship.
        /// </summary>
        public virtual Spaceship Spaceship { get; set; }

        /// <summary>
        /// Gets Icolletions.
        /// </summary>
        public virtual ICollection<Employee> Employees { get; }

        /// <summary>
        /// Gets MainData which helps to write employer data.
        /// </summary>
        public string MainData => $"[{this.Identichip}] : {this.Name} : {this.Homeworld} : (Has {this.CreditsOwn} credit) : (Bounty on its head: {this.Bounty} credit) : (Hair color: {this.HairColor})";
    }
}
